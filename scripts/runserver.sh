#!/bin/bash

base=$(readlink -f "${0}")
base="${base%/*/*}"
cd "${base}/docs"
[[ ! -z "${CLEAN}" ]] && rm -fr _build
make html
"${base}/scripts/add-timestamp.sh"
cd _build/html/
python3 -m http.server --bind 0.0.0.0 1313
