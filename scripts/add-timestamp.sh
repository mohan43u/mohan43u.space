#!/bin/bash

base=$(readlink -f "${0}")
base="${base%/*/*}"

find "${base}/docs/blog" -type f |
    while read f
    do
	t=$(echo "${f##*/}" | cut -d_ -f1 | grep '^[0-9]*$')
	c=$(stat -c '%Y' "${f}")
	[[ -z "${t}" ]] && echo "${f}" && mv "${f}" "${f%/*}"/"${c}"_"${f##*/}"
	[[ ! -z "${t}" ]] && [[ "${c}" != "${t}" ]] && echo "${f}" && touch --date="$(date -d @${t} +'%Y-%m-%d %H:%M:%S')" "${f}"
    done
