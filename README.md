# mohan43u.space

My personal website.

## Howto generate

```bash
$ git clone <this-repo>
$ cd <this-repo-dir>
$ scripts/bootstrap poetry shell
$ (cd ./docs; make html)
$ tar cvzf <this-repo>.tar.gz -C docs/_build/html .
```

Transfer the generated static website to whatever place you want and extract, then serve.
