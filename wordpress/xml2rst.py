#!/usr/bin/env python3

import os
import re
import sys
import time
import calendar
import xml.parsers.expat

item = 0
capturetitle = 0
capturepubdate = 0
capturedata = 0
ctitle = None
cpubdate = None
cdata = None

def start_element(name, attrs):
    global item
    global capturedata
    global capturetitle
    global capturepubdate
    global ctitle
    global cpubdate
    global cdata
    
    if name == "item":
        item = 1
    if item == 1 and name == "title":
        capturetitle = 1
        ctitle = ""
    if item == 1 and name == "pubDate":
        capturepubdate = 1
        cpubdate = ""
    if item == 1 and name == "content:encoded":
        capturedata = 1
        cdata = ""

def char_data(data):
    global capturedata
    global capturetitle
    global capturepubdate
    global ctitle
    global cpubdate
    global cdata

    if capturetitle == 1:
        ctitle += data
    if capturepubdate == 1:
        cpubdate += data
    if capturedata == 1:
        cdata += data

def end_element(name):
    global item
    global capturedata
    global capturetitle
    global capturepubdate
    global ctitle
    global cpubdate
    global cdata

    if item == 1 and name == "title":
        capturetitle = 0
    if item == 1 and name == "pubDate":
        capturepubdate = 0
    if item == 1 and name == "content:encoded":
        capturedata = 0
        
    if name == "item":
        item = 0
        if len(cdata) > 0:
            ptime = time.strptime(cpubdate, '%a, %d %b %Y %H:%M:%S %z')
            ptime_epoch = calendar.timegm(ptime)
            if not os.path.exists(str(ptime.tm_year)):
                os.makedirs(str(ptime.tm_year), 0o755)
            articlefile = ctitle.lower().replace(' ', '_')
            articlefile = re.sub('[^a-zA-Z0-9_]', '', articlefile)
            articlefile = str(ptime.tm_year) + '/' + str(ptime_epoch) + '_' + articlefile + '.rst'
            with open(articlefile, 'w') as article:
                article.write('.. title:: ' + ctitle)
                article.write('\n\n%s\n' % (''.join(['*' for n in range(len(ctitle))])))
                article.write(ctitle)
                article.write('\n%s\n' % (''.join(['*' for n in range(len(ctitle))])))
                article.write('\n| Updated: |modifieddate|\n\n')
                article.write(cdata)
            os.utime(articlefile, (ptime_epoch, ptime_epoch))


def main():
    parser = xml.parsers.expat.ParserCreate()
    parser.StartElementHandler = start_element
    parser.EndElementHandler = end_element
    parser.CharacterDataHandler = char_data
    parser.Parse(open(sys.argv[1]).read(), 1)

if __name__ == "__main__":
    main()
