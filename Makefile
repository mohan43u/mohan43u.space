outdir := $(shell pwd)/mohan43u.space

all: html pdf

genoutdir:
	mkdir -p $(outdir)

html: genoutdir
	(cd docs; make html; tar cvzf $(outdir)/mohan43u.space.html.tar.gz -C _build/html .)

pdf: genoutdir
	(cd docs; make latexpdf; cp _build/latex/mohan43usspace.pdf $(outdir)/mohan43u.space.pdf)
clean:
	make -C docs clean
	rm -fr $(outdir)

.PHONY: genoutdir html pdf clean
