.. title:: mohan43u's space

****
Blog
****

.. toctree::
   :name: blog
   :titlesonly:
   :glob:
   :reversed:

   blog/*/*

.. toctree::
   :name: index
   :titlesonly:

   index/aboutme.rst
