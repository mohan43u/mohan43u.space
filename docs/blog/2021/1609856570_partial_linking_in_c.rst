.. title:: Partial Linking In C

********************
Partial Linking In C
********************

| Updated: |modifieddate|


Its better to give an example first then explain. Here is the source code of a library called ``libplic_math.a``, it is a very simple library with addition(``plic_add``), subtraction(``plic_sub``) and multiplication(``plic_mul``) functions.

*plic_math.h*

   .. literalinclude:: /_static/misc/partial-linking-in-c/plic-math/plic_math.h
      :language: c
      :linenos:

*plic_add.c*

   .. literalinclude:: /_static/misc/partial-linking-in-c/plic-math/plic_add.c
      :language: c
      :linenos:

*plic_sub.c*

   .. literalinclude:: /_static/misc/partial-linking-in-c/plic-math/plic_sub.c
      :language: c
      :linenos:

*plic_mul.c*

   .. literalinclude:: /_static/misc/partial-linking-in-c/plic-math/plic_mul.c
      :language: c
      :linenos:


*Makefile*

   .. literalinclude:: /_static/misc/partial-linking-in-c/plic-math/Makefile
      :language: makefile
      :linenos:

Here is the source code of another library called ``libplic_sum.a``, this library uses addition(``plic_add``) function from ``libplic_math.a`` library and provides another function to sum the results(``plic_sum``).

*plic_sum.h*

   .. literalinclude:: /_static/misc/partial-linking-in-c/plic-sum/plic_sum.h
      :language: c
      :linenos:

*plic_sum.c*

   .. literalinclude:: /_static/misc/partial-linking-in-c/plic-sum/plic_sum.c
      :language: c
      :linenos:

Here is  the source code of one program ``plic_sum_elf`` which uses sum(``plic_sum``) function from ``libplic_sum.a``.

*plic_sum_elf.c*

   .. literalinclude:: /_static/misc/partial-linking-in-c/plic-sum/plic_sum_elf.c
      :language: c
      :linenos:

In ``plic_sum_elf.c``, The function ``plic_sum`` from ``libplic_sum.a`` depends on ``plic_add`` function from ``libplic_math.a``, so in order to generate ``plic_sum_elf`` executable, we have to link both ``libplic_sum.a`` and ``libplic_math.a`` like this,

.. code-block:: console

   $ cc -static -o plic_sum_elf plic-sum_elf.c -L/path/to/libplic_sum_dir -lplic_sum -L/path/to/libplic_math_dir -lplic_math
   
If we omit ``-lplic_math``, then compiler will throw error saying undefined reference to ``plic_add`` symbol. Imagine that you are writing one library like ``libplic_sum.a`` which itself depends on various libraries like ``libplic_math.a``, The user of your library also need to be aware of all the libraries which your library depends.

.. centered:: *It is unnecessary for the downstream user to know all the details about your library in order to link with your library*

.. topic:: Partial Linking with '-r' flag

   To simplify and make linking easier for your downstream user, you can provide your library as ``partially linked`` library, means, when you generate your library, you can `instruct the compiler through -r flag`_ to resolve all the symbols required for your library and generate one big object file in order to generate a partially linked library.

.. code-block:: console

   $ cc -r -o plic_sum_nodeps.o plic_sum.c -L/path/to/libplic_math_dir -lplic_math
   $ ar rcUs libplic_sum_nodeps.a plic_sum_nodeps.o

Now, it becomes easy to generate ``plic_sum_elf`` executable, no need to link with ``libplic_math.a``, downstream user only need to link with ``libplic_sum_nodeps.a``

.. code-block:: console

   $ cc -static -o plic_sum_elf plic_sum_elf.c -L/pat/to/libplic_sum_nodeps_dir -lplic_sum_nodeps

Here is the Makefile which generates ``libplic_sum.a``, ``libplic_sum_nodeps.a`` as well as ``plic_sum_elf`` executable.

*Makefile*

   .. literalinclude:: /_static/misc/partial-linking-in-c/plic-sum/Makefile
      :language: makefile
      :linenos:

|

.. target-notes::

.. _instruct the compiler through -r flag: https://gcc.gnu.org/onlinedocs/gcc/Link-Options.html
