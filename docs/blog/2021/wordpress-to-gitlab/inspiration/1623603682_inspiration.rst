.. title:: Inspiration

.. raw:: latex

   \newpage

===========
Inspiration
===========

Updated: |modifieddate|

I started working on improving `ILUGC`_'s online presence after `COVID`_ started. We renewed ILUGC's `irc`_ channel in `Freenode`_ after a long time (but again moved to `Libera.chat`_, that is another story), it was created around 2009 but nobody cared. During channel registration, freenode stuff asked me does ILUGC have any online presence. I provided ILUGC's `website`_, they asked me to add one `TXT`_ record to that website's domain.

At that time, I dint know who owned ILUGC's website, but know that someone from ILUGC community retrived and restored that expired domain few years back. I enquired about that with `shrini`_ and came to know that `cnu`_ is the person who retrived and restored that website. So I sent a mail to ``cnu`` asking to add ``TXT`` record which freenode sent to me. He added it immediately.

Joined ILUGC website development

   Then ``cnu`` added me as one of the member of `ILUGC project`_ in ``GitHub``. I came to know that ILUGC's website is actually a `Hugo`_ powered static website. I tried to write some article for that website and understood how that website is hosted with the help of `Netlify`_. I wrote one `article`_ explaining how one can contribute to ILUGC's website from the knowledge I gained.

.. centered:: *ILUGC's website and the way it was hosted without running a webserver inspired me to move my wordpress hosted blog to a static website.*

.. container:: rightalign

   To be continued..

|

.. target-notes::

.. _ILUGC: https://ilugc.in/about/
.. _COVID: https://en.wikipedia.org/wiki/COVID-19
.. _irc: https://en.wikipedia.org/wiki/Internet_Relay_Chat
.. _Freenode: https://freenode.net/
.. _Libera.chat: https://libera.chat
.. _website: https://ilugc.in
.. _TXT: https://en.wikipedia.org/wiki/TXT_record
.. _shrini: https://goinggnu.wordpress.com/
.. _cnu: https://cnu.name/
.. _ILUGC project: https://github.com/ilugc
.. _Hugo: https://gohugo.io/
.. _Netlify: https://www.netlify.com/
.. _article: https://ilugc.in/get-involved/
