.. title:: ModifiedDate Extension for Sphinx

.. raw:: latex

   \newpage

=================================
ModifiedDate Extension for Sphinx
=================================

Updated: |modifieddate|

Idea Behind the Extension
-------------------------

When writing article in `Hugo`_ you have to provide some date in ``date`` field so that ``Hugo`` can show it in the statically generated website. Every time you update one article, you have to manually update this field.

I was looking for some way to generate this date automatically in `Sphinx`_. There is a way to add current date using `|today|`_ substitution. I was looking for similar kind of substitution which is capable of automatically fetching the current file's modified date. But unfortunately, I'm not able to find any extension for ``Sphinx`` which satisfy my need. So, I decided why not write one?

ModifiedDate Extension
----------------------

So, I looked into the source code of ``Sphinx`` which handles ``|today|`` `substitution`_, after understanding that code, I started implementing my own extension to ``Sphinx`` and came up with my own substitution called ``|modifieddate|``. Here is my extension,

.. literalinclude:: /exts/modifieddate/__init__.py


Automating Updated Date
-----------------------

Adding ``|modifieddate|`` substitution is not enough to automate. Every time I update one article, the current modified date in the disk will change, but I need to preserve the timestamp when I create that article. So I wrote one small script which will look at the prefix of each article filename, If the filename contains a timestamp, then it compares the current modified date for that file in the disk with that timestamp from filename, if they are not equal, then the script will update the modified date to the timestamp from the filename. In this way, I make sure the modified date is always preserved to the timestamp when the article was written. Since the modified date in the disk and the timestamp from the filename got synced, ``|modifieddate|`` substitution always return the timestamp from filename as intended.

If there is no timestamp in the filename, then my script will add the current time to the filename, this will make sure the filename of the article contains the original timestamp when it was created.

.. centered:: *the date you see at the top of this article after "Updated:" field comes from |modifieddate| substitution*

.. container:: rightalign

   To be continued..

|

.. target-notes::

.. _Hugo: https://gohugo.io/
.. _Sphinx: https://sphinx-doc.org/
.. _|today|: https://www.sphinx-doc.org/en/master/usage/restructuredtext/roles.html#substitutions
.. _substitution: https://github.com/sphinx-doc/sphinx/blob/60203e34a4ea6b5a1e2c9279bfdeab697305d97e/sphinx/transforms/__init__.py#L114
