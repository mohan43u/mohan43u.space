.. title:: PDF integration

.. raw:: latex

   \newpage

===============
PDF Integration
===============

Updated: |modifieddate|


`Sphinx`_ not only capable of generating ``HTML``, but it can also generate ``PDF``. Instead of doing ``make html``, we can run ``make latexpdf`` to generate pdf document. To generate ``PDF``, Sphinx first converts reStructuredText to ``tex`` document then calls any one of tex engine (`PdfTeX`_, `XeTeX`_ or `LuaTeX`_). You can configure any one of this engine through ``latex_engine`` config variable in Sphinx's ``conf.py``. If you don't specify this variable, ``pdftex`` will be used by default.

Issue with PdfTeX
-----------------

``PdfTeX``, the default ``TeX`` engine in Sphinx works great if you are sticking with ``ascii`` encoding. But, it fails to render ``unicode fonts`` properly. There are some workarounds to improve ``PdfTeX``,  but I find out that ``XeTeX`` by default supports unicode. So I decided to use ``XeTeX``.

Dependencies for XeTeX
----------------------

We have to make sure the following packages installed in order to use XeTeX in `Ubuntu`_

.. code-block:: console

   $ sudo apt install texlive-xetex fonts-freefont-otf latexmk xindy

Switch Sphinx to use XeTeX
--------------------------

We have to instruct Sphinx to use ``XeTeX`` as the default latex engine in ``conf.py``

.. code-block:: python

   latex_engine = 'xelatex'

Font Integration in XeTeX
-------------------------

In ``Linux``, we use `fontconfig`_ to utilize fonts of different type. But in ``XeTeX``, we have to use `fontspec`_ and explicitly mention which font to use. It can be done in Sphinx using ``fontpkg`` key in  ``latex_elements`` dictionary in ``conf.py``

.. code-block:: python
   
   latex_elements = {
       'fontpkg': r'''
   \setmainfont{FreeSerif}[
       Path = /usr/share/fonts/opentype/freefont/,
       BoldFont = *Bold,
       ItalicFont = *Italic
   ]
   \setsansfont{FreeSans}[
       Path = /usr/share/fonts/opentype/freefont/,
       BoldFont = *Bold,
       ItalicFont = *Oblique
   ]
   \setmonofont{FreeMono}[
       Path = /usr/share/fonts/opentype/freefont/,
       BoldFont = *Bold,
       ItalicFont = *Oblique
   ]

.. centered:: *The generated PDF document is available in the sidebar*

.. container:: rightalign

   To be continued..

|

.. target-notes::

.. _Sphinx: https://http://sphinx-doc.org/
.. _PdfTeX: https://www.tug.org/applications/pdftex/
.. _XeTeX: https://www.tug.org/xetex/
.. _LuaTeX: http://www.luatex.org/
.. _Ubuntu: https://ubuntu.com/
.. _fontconfig: https://www.freedesktop.org/wiki/Software/fontconfig/
.. _fontspec: https://github.com/wspr/fontspec/
