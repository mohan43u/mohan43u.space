.. title:: Migrated my WordPress blog to GitLab

*************************************
Migrated my WordPress blog to GitLab
*************************************

Updated: |modifieddate|

Recently I moved my `blog`_ from `WordPress`_ to `GitLab`_, you might be asking 'wait, what? what do you mean by moved to GitLab? when GitLab started providing blogging?', GitLab don't provide blogs, but we can use GitLab to host a static website like `GitHub Pages`_. So, this is how I converted my WordPress blog to a blog like static website and hosted in GitLab.

.. toctree::
   :name: meetty
   :titlesonly:
   :glob:

   wordpress-to-gitlab/inspiration/*
   wordpress-to-gitlab/design/*
   wordpress-to-gitlab/vssue-issue/*
   wordpress-to-gitlab/modifieddate-extension/*
   wordpress-to-gitlab/export/*
   wordpress-to-gitlab/pdf-integration/*
   wordpress-to-gitlab/publish/*

|

.. target-notes::

.. _blog: https:/mohan43u.wordpress.com
.. _WordPress: https://wordpress.com
.. _GitLab: https://gitlab.com
.. _GitHub Pages: https://about.gitlab.com/stages-devops-lifecycle/pages/
