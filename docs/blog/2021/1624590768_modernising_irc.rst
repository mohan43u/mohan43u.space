.. title:: Modernising IRC

***************
Modernising IRC
***************

Updated: |modifieddate|

IRC Protocol

   `IRC`_ is one of the oldest protocol from the `Dinosaur Age`_ which is still alive. It is very simple, there is one ``IRC Server`` running somewhere in the Internet. You connect to that server using an ``IRC Client``, join one ``Channel`` (in other words a ``Room``), and start chatting with people in that channel.


Limitations

   Since ``IRC`` is very simple text based protocol, it doesn't have a mechanism to store the history, means, if you disconnect from the server, you no longer receive messages from other people in the ``Channel``. In Order to receive all the messages from a particular ``Channel``, your ``IRC Client`` need to stay connected with the ``IRC Server``.

   Also, we cannot watch what is happening in the ``Channel`` all the day. So, the ``IRC Client`` should have the ability to provide notification so that when you see the notification, you can decide whether you want to switch look at the conversation happening in the ``Channel`` or not. Most of the ``IRC Clients`` developed for ``Desktop Operating Systems`` do this notification without any issue. Also, they can stay connected with the ``IRC Server`` as long as you keep them running.

   ``IRC`` requires the client to be connected to the server through ``TCP Socket``, but modern mobile operating systems don't like apps which keep on running and stay connected through ``TCP Socket``, these modern mobile operating systems want the apps to use `Push Notification`_ to save battery, these mobile operating systems will kill the ``IRC Client`` process if they think that the process is not doing anything. Thus, ``IRC Clients`` developed for ``Mobile Operating Systems`` cannot effectively stay connected forever with ``IRC Server`` through ``TCP Socket``.

=======
WeeChat
=======

`WeeChat`_ is a terminal based ``IRC Client``. Just like any other ``IRC Client``, ``WeeChat`` provides all the functionality of an ``IRC Client`` and also provides functionalities more then the normal ordinary ``IRC Client``. We can extend ``WeeChat`` through plugins. Plugins can be written using ``C``, ``Python``, ``Perl`` etc. Infact, most of the core functionalities are implemented as plugins in ``WeeChat``.

=============
WeeChat Relay
=============

One of the plugin in ``WeeChat`` is called ``Relay``. This plugin can act as a proxy so that you can use another ``IRC Client`` to connect to ``WeeChat`` and ``WeeChat`` will connect to the actual ``IRC Server``. This plugin also provide another type of communication using `WeeChat Protocol`_, we can use this protocol and write a ``WeeChat Client`` which act like ``WeeChat`` but without using ``IRC Protocol``.

===============
WeeChat Android
===============

`WeeChat Android`_ is a ``WeeChat Client`` for `Android`_ which communicates with  ``WeeChat`` running in a remote machine through ``WeeChat Protocol``. ``WeeChat Android`` acts like ``WeeChat`` so we get a ``IRC Client`` for ``Android``. ``WeeChat Android`` provides all the functionality of ``WeeChat`` but unfortunately it also suffers from the battery optimization in the ``Android`` operating system. It cannot stay connected to the remote ``WeeChat`` all the time. Also, ``WeeChat Android`` not able to use ``Push Notification`` because of the `Proprietary Infrastructure`_ those ``Push Notifications`` depends on.

=============
IrssiNotifier
=============

`IrssiNotifier`_ is one ``Android`` App which uses ``Push Notification`` to receive messages from a remote server. The ``IrssiNotifier Servers`` which send those ``Push Notification`` through the ``Proprietary Infrastructure`` are open for any valid clients to send those messages from a remote server.

We have to first create one client through ``IrssiNotifier`` website. Using the ``Client Id``, we can develop one ``WeeChat`` plugin. This plugin will catch all the notification from ``WeeChat`` and send those notification messages to ``IrssiNotifier Servers``. Those ``IrssiNotifier Servers`` then send those notification messages to ``IrssiNotifier Android App`` in our ``Android`` mobile. In this way, we can receive all the notification from ``WeeChat`` in our Mobile.

===========
WeeNotifier
===========

`WeeNotifier`_ is a plugin for ``WeeChat`` which uses ``IrssiNotifier Infrastructure`` to broadcast notifications from ``WeeChat``. With the combined use of ``Weechat``, ``WeeChat Android`` and ``WeeNotifier``, we can emulate a ``Modern Messaging System`` on top of ``IRC Protocol``

======================
Steps to Modernize IRC
======================

These are the methods I used to improve my IRC experience on par with any ``Modern Messaging System`` especially in ``Android``.

* Lease one very small cloud VM to run ``WeeChat`` continuously. You can also use your Desktop/Laptop to run WeeChat, but we need some way for ``WeeChat Android`` running in the mobile to connect with ``WeeChat`` running in your Desktop/Laptop. You can ask your ``Internet Service Provider`` to provide a ``Static IP`` or you can use ``Dynamic DNS`` method to make your Desktop/Laptop available through Internet. In the end, all we need is to somehow make ``WeeChat Android`` connect to ``WeeChat``.

* Keep ``WeeChat`` running in that small cloud VM continuously.

* Configure ``WeeChat`` to generate notification for all the messages of a particular ``Channel``.

* `Configure WeeChat Relay`_ in ``WeeChat`` so that ``WeeChat Android`` can connect securely.

* Install ``WeeChat Android`` App in your Mobile and configure it to connect to your ``WeeChat`` running in the small cloud VM.

* Create an account in ``IrssiNotifier Infrastructure`` through `their website`_. You need to generate ``Client Id`` to use it in ``WeeNotifier`` plugin as well as in ``IrssiNotifier`` Android App.

* `Install WeeNotifier`_ plugin in ``WeeChat`` to send notification to ``IrssiNotifier Servers``. Configure it so that it is able to send data to the servers.

* Install ``IrssiNotifier`` App in your Mobile and configure it to connect to the ``IrssiNotifier Servers``. If you want, you can change the encryption key to a different one from the default. In this case, you have to also configure ``WeeNotifier`` plugin in ``WeeChat`` with the same encryption key.

* That's all. When ``WeeChat`` generate notifications, it will be captured by ``WeeNotifier`` and sent to ``IrssiNotifier Servers``, then ``IrssiNotifier Servers`` will push those notifications to ``IrssiNotifier`` App in your mobile, Once you see the notification, you can then open ``Weechat Android`` App in your mobile to connect back to ``WeeChat`` to see whats going in the ``IRC Channel``. Since, ``WeeChat`` is running continuously connected to actual ``IRC Server``, the history is preserved.

=======================
Why I wrote WeeNotifier
=======================

When I was looking for some way to do push notifications from ``WeeChat``, I came to know about IrssiNotifier and its Infrastructure. Immediately, I found that there is already one plugin available in ``WeeChat`` called ``IrssiNotifier`` which does exactly what I wanted. But when I tried, It didn't work, I looked it to fix it, then I saw that the plugin invokes ``openssh`` for every message to do encryption. I'm not agree to do this way. Invoking a subprocess for each and every message is not a good thing.

So, I decided to write my own plugin. I know that ``WeeChat`` will not accept competing plugins for same functionality, but I don't care whether ``WeeChat`` accepts or not. It is my own Itch which I scratched. I'm now happy with the result. I `submitted`_ my plugin to WeeChat anyway.

|

.. target-notes::

.. _IRC: https://en.wikipedia.org/wiki/Internet_Relay_Chat
.. _Dinosaur Age: https://en.wikipedia.org/wiki/IRCd#History
.. _Push Notification: https://firebase.google.com/docs/cloud-messaging
.. _WeeChat: https://weechat.org/
.. _WeeChat Protocol: https://weechat.org/files/doc/stable/weechat_relay_protocol.en.html
.. _Weechat Android: https://github.com/ubergeek42/weechat-android
.. _Android: https://www.android.com/intl/en_in/
.. _Proprietary Infrastructure: https://firebase.google.com/docs/cloud-messaging
.. _IrssiNotifier: https://irssinotifier.appspot.com/
.. _WeeNotifier: https://gitlab.com/mohan43u/weenotifier
.. _Configure WeeChat Relay: https://weechat.org/files/doc/stable/weechat_user.en.html#relay_plugin
.. _their website: https://irssinotifier.appspot.com/#profile
.. _Install WeeNotifier: https://gitlab.com/mohan43u/weenotifier#setup
.. _submitted: https://github.com/weechat/scripts/pull/466
