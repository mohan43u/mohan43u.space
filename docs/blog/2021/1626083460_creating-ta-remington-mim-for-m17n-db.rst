.. title:: Creating ta-remington.mim for m17n-db

*************************************
Creating ta-remington.mim for m17n-db
*************************************

Updated: |modifieddate|

=======
Problem
=======

One fine day, I was deep in my office work, Suddenly I got a ping from `shrini`_ in `#ilugc`_ asking me to take a look at this `issue`_. At first, I thought well, it is a simple change, why we need to write a whole new layout for this simple change. But then ``shrini`` insisted that even if it is a simple change, People who were not able to do that simple change are looking for a solution for long time. So, I said to myself, if that is the case, then lets create one.

=======================
Learning m17n-db format
=======================

I was not interested on just copying already available mim files and simply modify for our purpose. I wanted to learn what exactly the format is, so I learnt how the parser parses mim through this `link`_. I studied and understood what each and every word in that file represents.

========================
Writing ta-remington.mim
========================

Creating this file was fairly easy, I just took rules from `Ekalappai`_ and a simple ``M-x query-replace-regexp`` in my `emacs`_ did 80% of the job, after that, I just added headers and tail.

.. code-block:: emacs

   M-x query-replace-regexp
   Query-replace-regexp: ^\([^[:space]]+\)[[:space:]]+\([^[:space:]]+\)^J
   with: ("\1" "\2")

======================
Testing the new Layout
======================

I gave my changes to ``shrini`` to see if he can use it, I think he did the basic test and confirmed me that it is working as expected. Then he forwarded my changes to the original requester who wanted this layout. Thankfully, he also was satisfied with my changes.

=============
Creating Icon
=============

In order to upstream ``ta-remington.mim``, we also have to create one icon file for this new layout. With my limited knowledge in gimp, I was able to create one png file for ta-remington.mim.

==================
Pushing to m17n-db
==================

With my changes, I sent a `mail`_ asking them to include ta-remington.mim in `m17n-db mailing list`_. After few days I got a response that m17n-db `included my changes`_.

|

.. target-notes::

.. _shrini: https://goinggnu.wordpress.com/
.. _#ilugc: https://meet.ilugc.in
.. _issue: https://github.com/KaniyamFoundation/ProjectIdeas/issues/160
.. _link: https://www.nongnu.org/m17n/manual-en/m17nDBFormat.html
.. _Ekalappai: https://github.com/thamizha/ekalappai/blob/master/installer/app/keyboards/Tamil-typewriter.txt.in
.. _emacs: https://www.gnu.org/software/emacs/
.. _mail: https://lists.nongnu.org/archive/html/m17n-list/2021-06/msg00000.html
.. _m17n-db mailing list: https://lists.nongnu.org/mailman/listinfo/m17n-list
.. _included my changes: https://lists.nongnu.org/archive/html/m17n-list/2021-07/msg00003.html
