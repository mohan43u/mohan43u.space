.. title:: Emacs LSP for C

***************
Emacs LSP for C
***************

| Updated: |modifieddate|


LSP (Language Server Protocol)
==============================

`Language Server Protocol`_ is a standard way of assisting programmers by providing set of features (like on the fly compilation, symbol resolution, references resolution, providing help or documentation etc). This is a client/server model, implementation of this protocol consist of a server process (eg: `clangd`_) parsing the code which is currently written (or already written) and listening for any client to connect with it and ask for details. The client (eg: `lsp-mode`_ in `emacs`_) will communicate with the server on the fly and ask for details based on the current context of the code currently written.

clangd

   This LSP server is provided by `llvm`_ project for C/C++. Also, there are other LSP servers for C/C++ available like `ccls`_. This ccls depend on `libclang`_ which is also used by clangd, basically libclang is the one which parses the source code. Both ccls and clangd utilize the libclang library for code parsing.

lsp-mode

   This is a emacs package. This acts as a LSP client communicating with different LSP servers based on the current source file. There are additional packages like `lsp-ui`_ to provide visual response.

.. image:: /_static/images/lsp-mode.png
   :align: center
   :target: /_static/images/lsp-mode.png


Installation
============

LSP consist of two parts, the LSP server and the LSP client. For different languages, we need to install different LSP servers based on our requirement. On the client side, as we are going to use emacs, we only need to install lsp-mode (additionally lsp-ui) once, this same client can be used with multiple LSP servers for different languages.


Installing LSP Server for C/C++ (clangd)
----------------------------------------

For the C/C++ Language, we need to install clangd LSP server. This clangd is available on all major distros. You have to install based on your distro. Following command will install clangd in `Arch`_.

.. code-block:: console

   $ sudo pacman -S clang


Installing `bear`_
^^^^^^^^^^^^^^^^^^

Merely installing clangd will not going to provide all the LSP server functionality. You need to tell clangd about how-to parse each source file in your project so that clangd will be able to parse the source code same like the compiler will parse. To know how-to compile a source file, clangd uses `compilation database`_. This compilation database is nothing but a file called ``compile_commands.json`` which contains compilation command lines to compile each source code in the project. The `cmake`_ can generate this ``compile_commands.json`` automatically when ``-DCMAKE_EXPORT_COMPILE_COMMANDS=ON`` added to cmake. The `meson`_ can generate ``compile_commands.json`` automatically. But for non cmake or meson projects, bear tool is needed to produce ``compile_commands.json``. This bear tool is available on all major distros. You have to install based on your distro. For Arch, bear is available in `AUR`_.


Installing LSP Client for emacs (lsp-mode and lsp-ui)
--------------------------------------------------------------

Following emacs packages needed and can be installed through `ELPA`_/`MELPA`_ (``M-x list-packages``).

* lsp-mode (core LSP client)
* lsp-ui (adds UI to lsp-mode)
* `company`_ (text completion Framework for emacs)

Configuration
=============

Configuring clangd
---------------------

For clangd, we need to generate ``compile_commands.json`` file in project root directory. If you are using `autotools`_ based projects, instead of running `make`_, you have to run make with bear like this,

.. code-block:: console

   $ bear -- make

This will run make along with bear and records all the compilation commands from make, finally bear saves all the recorded compilation commands in ``compile_commands.json``.

Configuring emacs LSP packages
------------------------------

We need to add the following lines to `.emacs`_ to configure the LSP packages,

.. code-block:: lisp

   ;; company
   (add-hook 'after-init-hook 'global-company-mode)
   
   ;; lsp (language server)
   (require 'lsp-mode)
   (add-hook 'prog-mode-hook #'lsp-deferred)

Running emacs with lsp-mode
===========================

Just open a C/C++ source file or header file, lsp-mode will be automatically become active, emacs will also start clangd process. Following keybindings will be helpful once lsp-mode is active.

* M-. (to go to definition of the symbol at point)
* M-, (to traverse back in call stack)
* M-? (to list all references of the symbol at point)
* s-l G r (peek all references of the symbol at point)

.. image:: /_static/images/lsp-peek-references.png
   :align: center
   :target: /_static/images/lsp-peek-references.png

More `lsp keybindings`_ are available for lsp-mode.

|

.. target-notes::

.. _Language Server Protocol: https://microsoft.github.io/language-server-protocol/
.. _clangd: https://clangd.llvm.org/
.. _lsp-mode: https://emacs-lsp.github.io/lsp-mode/
.. _emacs: https://www.gnu.org/software/emacs/
.. _llvm: https://llvm.org/
.. _ccls: https://github.com/MaskRay/ccls
.. _libclang: https://clang.llvm.org/docs/Tooling.html
.. _lsp-ui: https://emacs-lsp.github.io/lsp-ui/
.. _Arch: https://archlinux.org/
.. _bear: https://github.com/rizsotto/Bear
.. _compilation database: https://clangd.llvm.org/config.html
.. _cmake: https://cmake.org/
.. _AUR: https://aur.archlinux.org/packages/bear/
.. _company: https://company-mode.github.io/
.. _autotools: https://www.gnu.org/software/automake/manual/html_node/Autotools-Introduction.html#Autotools-Introduction
.. _meson: https://mesonbuild.com/
.. _make: https://www.gnu.org/software/make/
.. _.emacs: https://www.gnu.org/software/emacs/manual/html_node/emacs/Init-File.html
.. _lsp keybindings: https://emacs-lsp.github.io/lsp-mode/page/keybindings/
.. _ELPA: https://elpa.gnu.org/
.. _MELPA: https://melpa.org/
