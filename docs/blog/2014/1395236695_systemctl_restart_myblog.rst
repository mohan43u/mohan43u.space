.. title:: SystemCtl Restart MyBlog

************************
SystemCtl Restart MyBlog
************************

| Updated: |modifieddate|

Its been 1yr 3months since I published my last post, lot of changes happened in my life. Just a quick look at my past

Marrage

   I got married on 06-Dec-2013 to Deepa (an introvert, but always want to be with her friends). Atlast, there is someone who will scold me when I don't eat/sleep/bath properly. I got a nice traveller now to travell with me rest of my life. My stomach is not burning when I go to multiplexes and malls after marrage :)

Moved to Pune

   Moved to beautiful Pune on Oct-2013. Nice city. But, I'm missing `ilugc <http://ilugc.in>`_ meetings very much. I have to start going to lug meeting in pune.

GStreamer

   Learnt `GStreamer <http://gstreamer.freedesktop.org/>`_ basics and wrote a module for `dhvani-tts <https://github.com/mohan43u/dhvani-tts>`_ which can use gstreamer for playing sound.

Well, nice to be back to blogging after a long gap.
