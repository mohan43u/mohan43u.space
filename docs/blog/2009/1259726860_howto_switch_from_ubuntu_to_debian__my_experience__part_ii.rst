.. title:: Howto switch from Ubuntu to Debian - My Experience - Part II

************************************************************
Howto switch from Ubuntu to Debian - My Experience - Part II
************************************************************

| Updated: |modifieddate|

continued...

Once I get the base system installed, I tried to hang around pure commandline for sometime, I installed the following package to hang around,

.. code-block:: console

   $ su -c 'apt-get install elinks'

Before going into aptitude for the final phase of my installation, I configured ``contrib`` ``nonfree`` and ``multimedia`` repos by creating two more files in ``/etc/apt/sources.list.d`` directory as below,

.. code-block:: console

   $ cat /etc/apt/sources.list.d/nonfree.sources.list 
   deb http://ftp.iitm.ac.in/debian/ lenny contrib non-free
   deb-src http://ftp.iitm.ac.in/debian/ lenny contrib non-free

   deb http://security.debian.org/ lenny/updates contrib non-free
   deb-src http://security.debian.org/ lenny/updates contrib non-free

   deb http://volatile.debian.org/debian-volatile lenny/volatile contrib non-free
   deb-src http://volatile.debian.org/debian-volatile lenny/volatile contrib non-free
   $

and

.. code-block:: console

   $ cat /etc/apt/sources.list.d/multimedia.sources.list 
   deb http://mirror.optus.net/debian-multimedia/ stable main
   deb-src http://mirror.optus.net/debian-multimedia/ stable main
   $

To use multimedia repo, we also need to install the gpg-key package using the following command,

.. code-block:: console

   $ wget http://www.debian-multimedia.org/pool/main/d/debian-multimedia-keyring/debian-multimedia-keyring_2008.10.16_all.deb
   $ su -c 'dpkg -i debian-multimedia-keyring_2008.10.16_all.deb'

Debian multimedia website provides more details, check this `link <http://www.debian-multimedia.org>`_

At the end, I started aptitude and installed all my must needed packages(Xfce,built-essential, gcc, vim-full, nvidia-glx, flash-10, mplayer, ffmpeg, w64codecs etc).

I don't want to run any services in my laptop, so I disabled all the default services in lenny(like exim4, avahi, gdm etc.,). The base system run with just ~34MB, Once I ``startx``, it goes to ~140MB, If I start ``icewesel``, it goes to ~314MB. I never thought I can run a full fledged system with this kind of less memory. No pause on Boot(just taking 10-12 sec to print login prompt)

Now, I'm really happy. I never expected the switch is this much smooth. Thanks to Debian.
