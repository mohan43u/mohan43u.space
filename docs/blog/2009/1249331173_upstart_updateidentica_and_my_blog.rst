.. title:: Upstart, update-identica and my blog

************************************
Upstart, update-identica and my blog
************************************

| Updated: |modifieddate|

Hi friends,

``Curl`` is one of the fantastic tool I'm using day to day. and Upstart is one good upgrade to the long standing ``rc.*`` method to start and stop services. While learning TwitterAPI to interact with ``identi.ca``, I got an idea to update my laptop's CPU usage to the ``identi.ca`` feed whenever my system starts and attach that feed into my blog. Well, It developed into something usable now. here is the script

.. code-block:: bash

   #!/bin/bash
   # update-identica - script to update my identi.ca feed
   
   USERID=""
   PASSWORD=""
   BASEURL="http://identi.ca/api"
   UPDATEAPIURL="/statuses/update.xml"
   UPDATEURL="${BASEURL}${UPDATEAPIURL}"
   MAXMESSAGES="10"
   LOGFILE="/var/log/update-identica.log"
   LOGFILESIZE=$(du -sb "${LOGFILE}" | awk '{print $1;}')
   MAXLOGFILESIZE=$((1024 * 1024))
   test "${LOGFILESIZE}" -gt "${MAXLOGFILESIZE}" && rm "${LOGFILE}"
   INTERVAL=$((5 * 60))
   
   destroymessages()
   {
   	EXTRAMESSAGES=$((MESSAGECOUNT - MAXMESSAGES))
   	EXTRAMESSAGEIDS=$(curl -u "${USERID}:${PASSWORD}" "${BASEURL}/statuses/user_timeline.xml" 2>/dev/null |
   	grep '[0-9]\{7\}' | cut -d'>' -f2 | tee -a "${LOGFILE}"
   }
   
   while true
   do
   	MESSAGE="status="
   	MESSAGE="${MESSAGE}[cpu:$(vmstat 1 2 | tail -1 | awk '{print $13+$14;}')%]"
   	MESSAGE="${MESSAGE} [uptime:$(uptime | cut -d',' -f1 | tr -d ' ')]"
   
   	curl -u "${USERID}:${PASSWORD}" -d "${MESSAGE}" "${UPDATEURL}" 2>/dev/null | tee -a "${LOGFILE}"
   	MESSAGECOUNT=$(grep '' -f2 | cut -d'<' -f1)
   	test '${MESSAGECOUNT}' -gt '${MAXMESSAGES}' && destroymessages
   	sleep '${INTERVAL}'
   done

Also call this script automatically by creating a event file /etc/events.d/update-identica

.. code-block:: bash

   # update-identica
   # a small program to update this laptop's feed in
   # http://identi.ca
   
   start on update-identica-event
   start on runlevel 2
   stop on runlevel [!2]
   exec /usr/bin/update-identica


Take a look at the feed titled ``My laptop's health`` in this blog, it is the feed which my laptop is updating every 5 minutes.

If you are interested in WebAPIs and accessing through ``curl``, then here is link, ``lucanica(identi.ca's software)`` also using the same REST methods to give web services.

http://apiwiki.twitter.com/Twitter-API-Documentation
