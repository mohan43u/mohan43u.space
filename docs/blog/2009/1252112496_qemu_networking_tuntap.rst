.. title:: Qemu Networking (Tun/Tap)

*************************
Qemu Networking (Tun/Tap)
*************************

| Updated: |modifieddate|

Setting up networking in Qemu instance is very simple if you are using ``user mode`` networking stack. And most of us don't care about Full fledged networking while using Qemu Guests. But configuring Network in Qemu with full access requires TUN/TAP configuration.

Here is some simple steps which I did to configure TUN/TAP in Ubuntu 9.04,

Inside Ubuntu(Host), type the following commands,

.. code-block:: console

   $ sudo apt-get install qemu kqemu-common kqemu-source
   $ wget ftp://ftp.osuosl.org/pub/lfs-livecd/lfslivecd-x86-6.3-r2145-min.iso
   $ sudo qemu -cdrom lfslivecd-x86-6.3-r2145-min.iso -boot d -kernel-kqemu -m 512 -net nic -net tap &
   $ sudo ifconfig tap0 192.168.10.1
   $ sudo sysctl -w net.ipv4.ip_forward=1
   $ sudo iptables -t nat -A POSTROUTING -j MASQUERADE
   $ sudo iptables -t filter -P FORWARD ACCEPT

Inside lfslivecd(Guest) type the following commands,

.. code-block:: console

   $ ifconfig eth0 192.168.10.2
   $ route add default gw 192.168.10.1
   $ cat > /etc/resolv.conf <<EOF
   nameserver 208.67.222.222
   nameserver 208.67.220.220
   EOF
   $

Ofcourse, you can use any distro as a guest in qemu. Enjoy Qemuing.
