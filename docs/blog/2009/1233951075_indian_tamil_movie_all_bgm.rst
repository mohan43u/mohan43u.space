.. title:: Indian Tamil Movie All BGM

**************************
Indian Tamil Movie All BGM
**************************

| Updated: |modifieddate|

Hi everyone,

Here is another post with BGM's of INDIAN movie directed by shankar, casting Kamal Haasan, Manisha koirala, Uurmila, Koundamani, matrum palar. A R Rahman established himself as the king of tamil music during this time. Enjoy his fantastic background scores.

These files are created using firefox's DownloadHelper plugin and mplayer. Most of the BGM's were flash contents. I simple play them, while playing, I'll copy the original flv file from /tmp directory or using DownloadHelper, then I'll go to the command prompt and convert that flv file to mp3 like this,

.. code-block:: console

   $ mplayer -dumpaudio -dumpfile 19SenapathyTheme.mp3 19SenapathyTheme.flv

Thats all. you got your mp3 file back from flv container.

But I'm really missing one BGM played when Senathipathy escaped from plane fire and saying ``If you currupt, Senathipathi will come back!!``. I'm searching for that BGM at the end. If someone know the link, kindly post here.

Updated (18/05/2009):

Hey, I got that final BGM, and 2 more extra, Man.. I searched for more than a year for this BGM(I-will-never-die.mp3), finally I got it.

Updated (22/06/2015):

Lost all the files which the above links pointing to. I have re-generated BGMs again (may not be the same ones as previous), here is the new links.

| `Indian.clip0.mp3 <https://drive.google.com/file/d/0B6Q-hqxJjfH7R0dkWVJnRzF2dDA/view?usp=sharing>`_
| `Indian.clip1.mp3 <https://drive.google.com/file/d/0B6Q-hqxJjfH7SGtjS1p2a0tNRlE/view?usp=sharing>`_
| `Indian.clip2.mp3 <https://drive.google.com/file/d/0B6Q-hqxJjfH7ak5KNmVZN0VZRDQ/view?usp=sharing>`_
| `Indian.clip3.mp3 <https://drive.google.com/file/d/0B6Q-hqxJjfH7dmR0MFNxRkdyLUk/view?usp=sharing>`_
| `Indian.clip4.mp3 <https://drive.google.com/file/d/0B6Q-hqxJjfH7TG5uTl9kZm15eU0/view?usp=sharing>`_
| `Indian.clip5.mp3 <https://drive.google.com/file/d/0B6Q-hqxJjfH7bjJlWVp3ZnpwWDA/view?usp=sharing>`_
| `Indian.clip6.mp3 <https://drive.google.com/file/d/0B6Q-hqxJjfH7N2VPbFRXSmNpQWs/view?usp=sharing>`_
| `Indian.clip7.mp3 <https://drive.google.com/file/d/0B6Q-hqxJjfH7R01LVjhXZlR4cFU/view?usp=sharing>`_
| `Indian.clip8.mp3 <https://drive.google.com/file/d/0B6Q-hqxJjfH7VWdsSERiY3d3YUU/view?usp=sharing>`_
| `Indian.clip9.mp3 <https://drive.google.com/file/d/0B6Q-hqxJjfH7czZKQ2ZseFlwdDQ/view?usp=sharing>`_

Enjoy..
