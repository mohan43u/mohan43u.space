.. title:: Kesavan's Birthday with ffmpeg and mencoder

*******************************************
Kesavan's Birthday with ffmpeg and mencoder
*******************************************

| Updated: |modifieddate|

Today is kesavan's birthday, shankar reminded us about this day before yesterday by telling that kesavan's birthday is on Feb 19. So I bags a cake and went to room. I also called stalin to come to room.

I reached home at 11pm, within few minutes, stalin also came. But kesavan and santhosh didn't come as per the timing. But we waited.

At 2am, santhosh and kesavan reached the room, and we wished kesavan with full of bumps, and beat him everywhere. He became frustrated and asked us to stop. We finally stopped and he asked ``why you are beating me?``. We said today is your birthday, and he said, ``Ada nadhari payabulligala, Innaiku illada nalaiku thaanda nan porandha naal``.

What a mess, But we said, ``Parava illada oru naal munnadi adikarathula onnum thappu illa``. Then we started to prepare for the celebration. Santhosh started to capture that moments in stalin's mobile(Its a Nokia 3500 classic).

Everybody enjoyed those moments and we captured that moment in mobile. Then kesavan, santhosh started to sleep, But something telling to me to do something with the video.

I transfered that video to my system and started to convert it to mpeg using ffmpeg, but it didn't work, because ffmpeg don't know howto decode smr audio stream. Thankfully mencoder knows howto decode smr audio. I used mencoder to convert that 3gp to mpeg4.

.. code-block:: console

   $ mencoder -oac lavc -ovc lavc -lavcopts acodec=libmp3lame:vcodec=mpeg4 -af scale=::::::qpal -o Video000.mp4 Video000.3gp

Converted that video to mp4 format. Now its time for ffmpeg to convert it into normal mpeg, because In my office, that windows media player don't know how to play mp4. So I decided to convert it to mpeg then to avi. Here is the commands,

.. code-block:: console

   $ ffmpeg -i Video000.mp4 -ar 44100 -ab 128k -acodec libmp3lame -vcidec mpeg2video -s qcif Video000.mpg
   $ ffmpeg -i Video000.mpg -ar 44100 -ab 128k -acodec wmav2 -vcodec msmpeg4 -s qcif Kesavan_Birthday.avi

everything went fine and we uploaded mpeg video to orkut through youtube and we mail .avi video to all our ignite friends.

That night is one dam good night for me. Here is the video,

.. raw:: html

   <iframe width="600" height="480" src="https://www.youtube.com/embed/msYn2S8Pa1M" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
