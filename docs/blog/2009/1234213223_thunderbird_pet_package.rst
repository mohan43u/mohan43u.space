.. title:: Thunderbird pet package

***********************
Thunderbird pet package
***********************

| Updated: |modifieddate|

Hi,

One more post, I compiled thunderbird 2.0.0.19 and created pet package using my new createpet script. That script is improving little by little. And it is doing good job so far.

I really stuckup with the .files file. First I taught it should be present with a pet package. But after seeing gcalculator.pet package, I realize that pet package manager is automatically creating that .files file. So I commented out and updated the new script.

I don't know whether it is good or bad. But I'm really improving it and it is paying off. Also the another script createsfs is going to be useful, because seniors are talking to automount .sfs files on the fly. It will be really interesting if puppy get that advantage over other distros.

Talking about thunderbird, boy!! it the longest compilation my AMD Athlon 1.8GHz machine did. The final pet size is 16MB. But with all headers and libraries.

So here is the link to get the latest thunderbird for puppy.

http://www.murga-linux.com/puppy/viewtopic.php?t=38542
