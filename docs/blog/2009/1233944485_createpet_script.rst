.. title:: createpet script

****************
createpet script
****************

| Updated: |modifieddate|

After rearranging my partitions and installing puppy, I got tha hunger to build .pet packages one more time(Ya, I really enjoyed compiling and building pet packages using puppy through my pendrive). One mistake I did previously is, I didn't put puppy into my hard disk. Yes, Its really a big mistake I did. It prevent me to interact more with puppy and diverted me to somewhere else.

But from last week, I'm always boot straight to puppy, from my experience, I would suggest, If you want real geekness, like how gentoo guys do, or how arc linux guys do, or how slackware guys do, then install puppy into your harddisk. Puppy is a perfect platform to do stuffs like compiling packages, hunting for dependencies, rebuilding, packing stuffs.

While using puppylinux, I felt that I'm missing my favourite browser, yes, firefox. So I download the source and started compiling, knowing that it will ask for different libraries, I fullfilled all the gaps by downloading and compiling the dependencies and finally got firefox 3.0.6 running in puppy 4.11.

To create packages, puppy have two scripts, one is new2dir and dir2pet. I used them on beginning, and lost interest with them. They are not giving enough flexibility to create .pet packages, So I went through dir2pet package and noted down what all files that script creating.

Then I started my own script to create .pet package from DESTDIR directory. After some initial testing I started to use createpet to create .pet packages. Here are the steps you need to do to create .pet package from source tarball using createpet script,

.. code-block:: console

   $ tar xvzf package-version.tar.gz
   $ cd package-version
   $ ./configure --prefix="/usr" --sysconfdir="/etc" --localstatedir="/var"
   $ make all
   $ make install DESTDIR="`pwd`-i686"
   $ cd ..
   $ createpet package-version-i686

or if you want a menu entry, then

.. code-block:: console

   $ createpet -m package-version-i686

Thats all, createpet will ask you some question. and finally your pet package will be ready with the name package-version-i686.pet. I also posted this script in puppylinux forum and people started to download. I'm waiting for someone to suggest something about the script.

You can also try this script and suggest me some improvements.

http://www.murga-linux.com/puppy/viewtopic.php?t=38311
