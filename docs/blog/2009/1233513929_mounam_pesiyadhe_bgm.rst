.. title:: Mounam Pesiyadhe BGM

********************
Mounam Pesiyadhe BGM
********************

| Updated: |modifieddate|

Ooh I love this music, Yuvan you are marvalous!! This is one of the best (I can say it is the best) from you!! keep rocking!!

Today suddenly I heared this BGM music one more time from Mounam Pasiyadhae, an unforgettable film I ever watched, the background music will take anyone to heaven. I have all the songs of Mounam Pasiyadhae, but don't able to get this BGM music.

sometimes back, I learnt howto use ffmpeg, ffmpeg2theora, mplayer and mencoder, so I decided to download entire movie  and do some stuff to convert that music into mp3.

While downloading the first part, my temper is out, I desperately want to convert that amazing background music immediately, first I played the downloading movie part with ``mplayer``, enabling(press key ``o``) OSD output. ``mplayer`` started to play the movie, man!!! what a music!! I will hear this 2min music for my lifetime :), OSD showed ``00:02:12`` as its passed time, I decided to use mplayer with ``-dumpaudio`` ``-dumpfile`` and ``-endpos 00:02:12`` to dump audio only for that duration, but ``mplayer`` did differently. It dumped the whole audio upto the downloaded part of the movie. Damn!!

So its fallback time, I decided to go to friendly beast ffmpeg, here is the command I did,

.. code-block:: console

   $ ffmpeg -i TamilBlast.Com_Mounam_Part\ I.avi -t 00:02:12 -f mp3 -ab 128k Mounam_Pasiyadhae_BGM.mp3

Thats all, I got the perfect duration of that music. All ends well and I'm writting this post while hearing this BGM music, thana.. naa.. naa.. naananaa.. naaa.. na!!!

`Mounam Pasiyadhae BGM.mp3 <https://drive.google.com/file/d/0B6Q-hqxJjfH7eXJyNi1GUzRhSXM/view?usp=sharing>`_
