.. title:: FossConf2009(Updated)

*********************
FossConf2009(Updated)
*********************

| Updated: |modifieddate|

Hi everyone,

Last week went with full of work pressure, No time to do anything for linux. With little time, I prepared for the FossConf 2009.

While preparing, I learnt a lot from gcc, autotools and lot others. Now I have some really good exposure about gcc and its tools. I feel very happy about going to madurai.

I already told to my PL about getting leave on friday, he also accepted it. I had some deep sleep on friday afternoon. Then I started my journey at 5pm. Exactly 8.05pm the bus started from Koyambedu Bus terminal.

On saturday morning 4am, I reached Trichy, then after 3 hours, I reached Maatuthavani Bus terminus in madurai. After some little journey in TownBus, I reached Thiyagaraja Engineering College, near Thiruparangundram, Madurai.

One of the student received me and gave me a seperate room. While going to the college, someone called me, What a surprise, Amachu standing near the Thiruparangundram bus stop, I went near to him, He asked about my journey and suggested me to call Bharathi and inform him.

Once I went into the room, I felt tired and sleepy, but somehow I was awaken and had a small trial on my presentation. Then it became boring, I started to look around the confrence.

Man!! lot of school students, participating and enjoying, Baskar(linuXpert) had full of attention towards those students. ``I can even take these students if they don't want to study Higher degrees, I can give the same salary a BE guy can get, to these students, b'se they worth equal``, he said to me.

Then I saw Amachu's Ubuntu-tam stall, So I felt that I can be a part of Ubuntu-tam. I was a person after Amachu who is helping him a lot. I introduced myself to him, by surprise, he is padu, the same padmanaban from pollachi. After sometime, amachu went away from the stall, me and padu handled students and visitors to our stall. Sametime, padu shared his experience with me, and I shared my happiness with him. One great thing is, we both are Puppy Lovers.

The afternoon 2pm I started my session, I felt disappointed about the awareness of the students, especially BE students about gcc, and its tools. So I quickly finished my session and Bharathi took from where I finished.

We both finished the sessions. Amachu asked me to give company while returning to chennai. We both started Journy at 5pm. On the way we met lot of persons from ``Yahoo!!, Kerala University, DrunkenMan etc``. We boarded SETC Airbus. The Bus started exactly 6.45pm from Maatudhavani Bus Terminus. While travelling, me and amachu exchanged our views about FOSS, Tamil Culture, Upper Middle Class, Respecting others culture, culture clashes in Metropolitan cities like mumbai, bangaluru. Amachu said that he also expects this kind of clash in Chennai.

At last, at 5.45am, we reached thambaram. All in all, this weekend was a meaningful to me. I met lot of FOSS enthusiasists and Also I teached something to the students.

After all the good things, I came to hear about NRCFOSS and their .ppt presentation. A lot of discussion going on in ILUGC mailing list.

In my openion, Whatever the reason is, if an organization is named for giving awareness about FOSS, then why the people working on NRCFOSS not using FOSS tools? How they will make awareness to others? And that person KG should change his attitude when posting in mailing list, his response to Arun SAG mail is not at all acceptable.

Anyway, here is my presentation. Hope this will help someone to program using C in Linux.

.. raw:: html

   <iframe src="//www.slideshare.net/slideshow/embed_code/key/6Vf0xK6hJV37s" width="595" height="485" frameborder="0" marginwidth="0" marginheight="0" scrolling="no" style="border:1px solid #CCC; border-width:1px; margin-bottom:5px; max-width: 100%;" allowfullscreen> </iframe> <div style="margin-bottom:5px"> <strong> <a href="//www.slideshare.net/mohan43u/c-under-linux" title="C Under Linux" target="_blank">C Under Linux</a> </strong> from <strong><a href="https://www.slideshare.net/mohan43u" target="_blank">mohan43u</a></strong> </div>
