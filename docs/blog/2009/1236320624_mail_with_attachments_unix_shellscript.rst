.. title:: Mail with Attachments (Unix shellscript)

****************************************
Mail with Attachments (Unix shellscript)
****************************************

| Updated: |modifieddate|

Lot of us already know howto send attachments in mails using shellscript. Lot of us already wrote these kind of script. Here is one more script to do the same thing. I already added all the details in the script itself. It is self explainable. If somebody wants, take it.

.. code-block:: bash

   #!/bin/sh 
   # generatemail.sh - script to send mail with multiple attachments.
   # Wrote By: me! mohan43u.
   # Date: Thu Mar 06 11:56:12 IST 2009
   # License: Its in public domain. Use it whatever way you can.
   
   USAGE="USAGE:
   
   	generatemail.sh [-t tolist] [-s subject] [-e encodetype] 
   		[-h] ATTACH_FILE1 ATTACH_FILE2 ...
   "
   HELP="${USAGE}
   OPTIONS:
   	-t tolist	comma seperated e-mail addresses
   				(Default: current userid).
   
   	-s subject	Subject line (Default: 'test mail from generatemail.sh
   				 script')
   
   	-e encodetype	Encoding type (x-uuencode/base64). only GNU uuencode
   			can encode in base64. If you are using this script
   			in Old school unixes(solaris, HP-UX, AIX, etc) use
   			only 'x-uuencode'. Linux can handle both types
   			(Default: x-uuencode).
   
   	-h		Print this Help text
   
   This script will read standard input as a mail body and takes filenames as 
   arguments to generate a preformatted mail content in satndard output. you can
   simply pipe the output to any MTA (sendmail, postfix etc.,) to send a mail
   with attachments.
   
   Eg:
   	$ generatemail.sh -t foo@bar.com -s 'mail with attachments' file1 \
   		file2 | /usr/sbin/sendmail -v -i -t
   	hi foo,
   		I attached file1 and file2 with this mail. Take a look at it.
   
   	Thanks,
   	ben.
   	[CTRL-D]
   	$
   
   	This above command line will attach file1 and file2 and send a mail
   	to foo@bar.com. The files file1 and file2 will be encoded in
   	'x-uuencode' and attached with this mail.
   "
   
   TO="${USER}"
   SUBJECT="testmail from generatemail.sh script"
   BOUNDARY="`date +'%G%m%d%H%M%S'`"
   ENCODE_TYPE="x-uuencode"
   
   while getopts 't:s:e:h' OPTIONS
   do
   	case "${OPTIONS}" in
   	t) TO="${OPTARG}";; 
   	s) SUBJECT="${OPTARG}";;
   	e) ENCODE_TYPE="${OPTARG}";;
   	h) echo "${HELP}" && exit 0;;
   	\?) echo "${USAGE}" && exit 1;;
   	esac
   done
   
   SHIFT_COUNT=`expr "${OPTIND}" - 1`
   shift "${SHIFT_COUNT}"
   
   header()
   {
   	echo "To: ${TO}"
   	echo "Subject: ${SUBJECT}"
   	echo "Mime-Version: 1.0"
   	echo "Content-Type: multipart/mixed; boundary=\"${BOUNDARY}\""
   	echo "Content-Disposition: inline"
   	echo "User-Agent: generatemail.sh 0.1"
   }
   create_octet_part()
   {
   	BASENAME=`basename "${1}"`
   	FILE="${1}"
   
   	if test "${ENCODE_TYPE}" = "base64"
   	then
   		ENCODED_MESSAGE=`uuencode -m "${FILE}" "${BASENAME}" \
   		| egrep -v '^begin|^='`
   	else
   		ENCODED_MESSAGE=`uuencode "${FILE}" "${BASENAME}"`
   	fi
   
   	echo "Content-Type: application/octet-stream"
   	echo "Content-Disposition: attachment; filename=\"${BASENAME}\""
   	echo "Content-Transfer-Encoding: ${ENCODE_TYPE}"
   	echo
   	echo "${ENCODED_MESSAGE}"
   	echo
   }
   create_text_part()
   {
   	echo "Content-type: text/plain; charset=utf-8"
   	echo "Content-Disposition: inline"
   }
   
   # Main
   
   BODY="`cat`"
   header
   echo
   echo "--${BOUNDARY}"
   create_text_part
   echo
   echo "${BODY}"
   echo
   for FILENAME
   do
   	echo "--${BOUNDARY}"
   	create_octet_part "${FILENAME}"
   done
   echo "--${BOUNDARY}--"
