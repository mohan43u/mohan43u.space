.. title:: Starting Another X Server with Different Locale - Ubuntu

********************************************************
Starting Another X Server with Different Locale - Ubuntu
********************************************************

| Updated: |modifieddate|

While playing with quemu+lfslivecd under ubuntu, I came to know about the following commands which sets the locale environment for a linux system(atleast for lfslivecd). Using the following steps, I'll share the commands and start a new X server with the modified environment.

1. Press ``ctrl+alt+F1`` to go to virtual terminal 1 or ``tty1`` (To switch back to current gnome session, use ``ctrl+alt+f7``). Login to that terminal with different userid then the current userid who already logged into current gnome session.

2. type the following command to set the timezone. you can see the timezone data file in /usr/share/zoneinfo/posix/{continent}/{country} where {continent} is the name of your continent like ``Europe``, ``Asia`` etc., and {country} is your country name. This file should be pointed by TZ environment variable. The below command will change Timezone to ``Europe/Spain``.

   .. code-block:: console

      $ export TZ="Europe/Spain"
      $ date

The date command will show the date according to the new timezone.

3. ``localedef`` command is used to create binary definitions in /usr/lib/locale directory from /usr/share/i18n/ directory, the ``i18n`` directory contains ``charmaps`` directory which contains encodings(like ``UTF-8`` etc). ``i18n`` also contains ``locales`` directory which contains definitions for locales. ``localedef`` will use these two directories to generate a definiton for particular language termed in ``{LANG}_{COUNTRY}.{ENCODING}`` (eg: es_ES.utf8 - means language code ``es`` (spanish), country ``ES`` (spain) and encoding in ``utf8``). The following command will compile language definition for spanish language in ``utf8`` encoding.

   .. code-block:: console

      $ sudo localedef -f UTF-8 -i es_ES --no-archive 'es_ES.utf8'
      $ locale -a | grep 'es_ES.utf8'

   ``locale -a`` will show whether language definition added or not.

4. Now we are ready to change the LANG variable, this variable is used by all ``glibc's language aware`` applications to switch their output to a particular language, the ourput strings for a particular application for a particular language should be available in ``{application}.mo`` file inside /usr/share/locale/{LANG}/LC_MESSAGES directory(Distros provide ``.mo`` files through language specific packages). Now switch the glibc applications to spanish using the following command,

   .. code-block:: console

      $ export LANG="es_ES.utf8"
      $ locale

   ``locale`` command will tell the current language.

5. Finally start a new X server using the following command with modified locale. You can switch back to old X server using ``ctrl+alt+F7``. To switch to new X server, use ``ctrl+alt+F9`` (or ``ctrl+alt+F10`` if it started in ``tty10`` virtual terminal)

   .. code-block:: console

      $ startx -- :1 -br -audit 0 -nolisten tcp 

   At first, it will ask to change the name of the directories to suite the new language, don't rename unless you always going to work in the new locale environment.

6. Once you done, ``Logout`` from the New X server(new gnome session), it will stop the newly started ``X server`` and put you in ``tty1`` virtual terminal. ``exit`` command will exit you from the tty1 terminal. Finally, switch back to old X server's gnome session using ``ctrl+alt+F7``.

Thats all.
