.. title:: Dharmastala - Where you see yourself

************************************
Dharmastala - Where you see yourself
************************************

| Updated: |modifieddate|

Hi friends,

Its my grandma's wish, that I should wipe my hair to god Manjunatha, who is there in dharmastala. My family planning to go and finish the pilgrimage, but time didn't allowed us, last week, we finally decided and started our journey.

Last Wednesday morning 3.30 am, we started, but there is no bus to banglore from my village pallikonda until 5.00 am.(Actually every bus to Bangalore will cross my village). So we went to Vellore and the boarded bus to Bangalore. Afternoon 11.55, we reached majestic.

The we had lunch, then we boarded ``Dharmastala`` but at 1.00 pm, after a long journey, we reached ``hassan`` at 5.45 pm, ``hassan`` is a beautiful and well planned city, which is center to mangaluru and Bengaluru(sorry, Bangalore was changed to bengaluru). The real journey started after ``hassan``, full of forest, full of waterfalls and kerala style houses gives so much pleasure to our eyes.

Also the way the drivers drive the bus is simply fantastic, my dirver constantly driving the bus with 60km even in the danger curves, I really enjoyed his driving. If you wana roller coster ride, go to ``dharmastala``, you will definitely enjoy the bus ride.

We reached that beautiful temple at 9.55 pm, then as usual, I booked a dharma room and we had dinner there. Morning 4.00 am, me and my father went to wipe my hair, it took nearly 3 hours to finish that proccess. Then our family went inside temple at 9.30 am, and came out 11.45 am. Then we went to ``Dharma Saapadu``, means afternoon meals. Its just rice with ``rasam``, but it tasts like ``amirdham``. I already know about the afternoon meals, but I felt the sweetness of the meals at realtime. It was simply superb.

We started our return at 2.00 pm afternoon, and reached home at 4.00 am. All in all, it was a beautiful time, I spend with my family, after my college studies.

.. raw:: html

   <iframe src="https://www.google.com/maps/embed?pb=!1m26!1m12!1m3!1d1569219.53000377!2d76.03925146396196!3d12.802710761350278!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m11!3e6!4m3!3m2!1d12.912144099999999!2d78.93774739999999!4m5!1s0x3ba4c879ec0d1f0f%3A0x165b33719d1e3337!2sDharmasthala%2C%20Karnataka!3m2!1d12.9545854!2d75.3810601!5e1!3m2!1sen!2sin!4v1623008476337!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
