.. title:: Share your Qemu Guest with your Friends

***************************************
Share your Qemu Guest with your Friends
***************************************

| Updated: |modifieddate|

Don't know about the how useful it will be, but I found some way to enable my friends to access my Qemu Guest Instance running LFSLiveCD in Ubuntu Host using ``ssh``. Here is the steps I did,

1. Download Qemu Packages

.. code-block:: console

   $ sudo apt-get install qemu kqemu-common kqemu-source

2. Download `LFSLiveCD from here <http://www.linuxfromscratch.org/livecd/download.html>`_

3. Now start the LFSLiveCD guest using the following command

.. code-block:: console

   $ qemu -cdrom lfslivecd-x86-6.3-r2145-min.iso -boot d -m 512 -net nic -net user -redir tcp:35000::22

4. Now Qemu will start ``LFSLiveCD`` with user mode network stack, that is, from LFSLiveCD, ip 10.0.2.2 will be the Default gateway and DHCP server. ip 10.0.2.3 will be the DNS server and the LFSLiveCD will get 10.0.2 15. If you look ``netstat`` output in Host(here Ubuntu), there will be a line which tells port ``35000`` is listening. Qemu process is listening in that port, it will redirect connections from Host(Ubuntu's) 35000 port to Guest(LFSLiveCD's) 22(ssh) port. All we need to do is, start ``ssh`` service in LFSLiveCD Guest. Type the following command in Qemu.

.. code-block:: console

   [root:]# cd /etc/rc.d/init.d
   [root:]# ./sysklogd start
   [root:]# ./sshd start
   [root:]# passwd
   New Password: [newrootpassword]
   Retype New Password: [newrootpassword]
   [root:]# tail -f /var/log/*.log

Now you have ``sshd`` running in the guest, and you also changed the ``root`` password to a known password. All you need to do is, ask your friends to ``ssh`` to your Host's(Ubuntu) ip address but with ``35000`` port instead of default ``22`` port. And ask them to login as ``root`` as userid and ``[newrootpassword]`` as password. Your friends will be so much happy to see thay are became ``root``. In the mean while you can also login to your guest with ``ssh root@127.0.0.1 -p 35000``. If you want, add a new user and share that userid and password to your friends, so that they can have less power.


Instead of Ubuntu Host, We can do this in Windows XP, as a Normal powerless user. The below thing you need to do,

1. Download QemuManager `Zipfile Edition from here <http://www.davereyn.co.uk/qem/qman60.zip>`_
2. Download `LFSLiveCD from here <http://www.linuxfromscratch.org/livecd/download.html>`_
3. Configure LFSLiveCD inside QemuManager(follow this `Guide <http://www.davereyn.co.uk/qem/qemumanager6.pdf>`_).
4. While configuring Network, DoubleClick ``NetWork Card 1``, GoTo Port Redirectors tab, type ``35000`` in ``Port Visible to Host`` and ``22`` in ``Port Visible On Guest`` and click ``Add`` button.
5. Start ``LFSLiveCD`` Qemu Guest. and Do all changes I explained above to start ``sshd`` inside LFSLiveCD.
6. Download `Putty.exe from here <http://the.earth.li/~sgtatham/putty/latest/x86/putty.exe>`_
7. Start ``Putty.exe`` with Host as ``127.0.0.1`` and Port as ``35000`` and Connection Type as ``ssh``.
8. Login with ``root`` as username and ``[newrootpassword]`` as your password.
9. If you want to share your Qemu Guest, with your windows friends who's machines and your's machine are in same subnet, then ask them to start ``putty.exe`` with host as your machine's ip address, port as ``35000``, username as ``root`` and password as ``[newrootpassword]``. You can also create seperate ``user`` in your Guest(LFSLiveCD) and share with your friends.
10. Enjoy Virtualization.

I also tried this method in my friend's Vista laptop, but it failed!! First, it is not allowing to run QemuManager.exe as a normal user, Second, it is giving warning while opening ``35000`` as listening port, Finally, ``putty.exe`` is not at all connecting to Guest Instance, even though ``sshd`` is running in the LFSLiveCD Guest. If anyone have success story, kindly share.

Finally Thanks to my company for blocking access to the linux learning server, which make me to go for alternate methods and finally endup in the Windows XP method.
