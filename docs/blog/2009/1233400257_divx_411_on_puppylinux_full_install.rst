.. title:: divx_411 on Puppylinux Full install

***********************************
divx_411 on Puppylinux Full install
***********************************

| Updated: |modifieddate|

Day before yesterday, I installed my puppy 4.11 into my harddrive. But the thing is, I don't know where to put divx_411.sfs file and make puppy load when it boots.

So I decided to extract divx_411 and copy it into my filesystem, here are the steps.

at ``/root`` directory,

.. code-block:: console

   $ mkdir divx_411_directory
   $ mount -o loop -t squashfs divx_411.sfs divx_411_directory
   $ cp -Rv divx_411_directory/* /
   $ umount divx_411.sfs
   $ rmdir divx_411_directory

Thats all, I got ``gcc`` back into my puppy. Need to build firefox-3.0 from source, I'll post it once I finish.
