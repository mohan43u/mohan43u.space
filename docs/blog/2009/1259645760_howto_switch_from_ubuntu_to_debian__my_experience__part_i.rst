.. title:: Howto switch from Ubuntu to Debian - My Experience - Part I

***********************************************************
Howto switch from Ubuntu to Debian - My Experience - Part I
***********************************************************

| Updated: |modifieddate|

After long time I'm posting this. In this time, I got a fantastic experience of switching myself from ubuntu to debian. Reasons? may be silly but here they are,

1. From 8.10, I had a tough time with all the kernel's ubuntu shipped and updated into my laptop, I was hit by a kernel acpi `bug <http://bugzilla.kernel.org/show_bug.cgi?id=11727>`_) which still exists(in i686, not in x86_64). It pauses boot process when my laptop runs in battery and I need to press until init starts.

2. Nowadays, I hate the idea of ``linux for everyone``. because ``nix`` systems are not for everyone. its for the one who like to learn computing.

3. My laptop lost DVD drive.

   So I was in a mood to switch myself to something else. While surfing, I came to the debian website, their Installation guide explains howto install debian from a `USB <http://www.debian.org/distrib/netinst#verysmall>`_. Mine is Athlon64, so this time I went for x86_64. First I prepared my pendrive. For that we need to partition it using ``fdisk``

   .. code-block:: console

      $ sudo fdisk /dev/sdb

   You can do this by running system->administration->partition-editor in Ubuntu. Once you created a new partition in your pendrive(/dev/sdb1), you need to format the partition as FAT32 using the following command

   .. code-block:: console

      $ sudo mkfs -t vfat /dev/sdb1

   Once you formatted, the next step is to install ``syslinux`` into it, If your Ubuntu don't have ``syslinux`` command, install ``syslinux`` package

   .. code-block:: console

      $ sudo apt-get install syslinux
      $ sudo syslinux /dev/sdb1

   Now its time to download the installation files, I copied all the files from the `following location <http://http.us.debian.org/debian/dists/lenny/main/installer-amd64/current/images/hd-media/>`_ and copied into /dev/sdb1. Finally the drive will look like this,

   .. code-block:: console

      $ find /media/disk
      .
      ./gtk
      ./gtk/vmlinuz
      ./gtk/initrd.gz
      ./syslinux.cfg
      ./vmlinuz
      ./initrd.gz
      ./ldlinux.sys
      ./MANIFEST
      ./boot.img.gz

   And it is capable of booting using ``syslinux`` bootloader. To use Graphical Installer, we need to modify ``syslinux.cfg``  to look like this

   .. code-block:: console

      default gtk/vmlinuz
      append initrd=gtk/initrd.gz video=vesa:ywrap,mtrr vga=788

   Once your pendrive is ready, unmount it and reboot the system, change the Boot order(In Compaq, press F9) and select the USB drive. The debian-installer will start asking questions, there on, its an easy ride. I didn't install Gnome/Kde when the installer asked to choose one. I was interested in Xfce, so I postponed it.

In next post, I'll tell you how I installed the 3rd layer(desktop).

will continue..
