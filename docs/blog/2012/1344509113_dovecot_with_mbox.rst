.. title:: Dovecot (with mbox)

*******************
Dovecot (with mbox)
*******************

| Updated: |modifieddate|

There are different ways to save a mail in \*nix systems. The old baddy is `mbox <http://en.wikipedia.org/wiki/Mbox>`_ which saves all your email in one single file. The new one is `Maildir <http://en.wikipedia.org/wiki/Maildir>`_.

Even though mbox have difficulties, traditional commandline mail clients like ``mailx`` works with ``mbox`` style mail files flawlessly. Also postfix which I setup in my :ref:`postfix` article by default creates single mbox mail file inside ``/var/spool/mail/`` directory for each user. So, I need a imap+pop3 server which can operate with mbox like mail files. `Dovecot <http://dovecot.org/>`_ is the choice.

.. image:: /_static/images/2012/08/dovecot-logo.png
   :target: http://dovecot.org/
   :align: center

First we need to install dovecot. I'm using sabayon, the instruction is specific to sabayon/gentoo,

.. code-block:: console

   $ sudo equo install net-mail/dovecot

Its time to setup dovecot. Before we do, we need to create self signed certificates to use it for imaps and pop3s. Lets first create those certificates.

.. code-block:: console

   $ cd /etc/ssl/dovecot
   $ sudo mkdir oldcerts
   $ sudo mv * oldcerts
   $ sudo openssl genrsa -out server.key.password -des3 1024
   Generating RSA private key, 1024 bit long modulus
   ..........................................++++++
   ..................................................++++++
   e is 65537 (0x10001)
   Enter pass phrase for server.key.password:
   Verifying - Enter pass phrase for server.key.password:
   $

We have to give ``pass phrase`` here, otherwise openssl command will not create key file. But, having pass phrase for key file is not good, because we need to provide this pass phrase everytime dovecot access this file. Here is the step to remove the pass phrase from rsa key file

.. code-block:: console

   $ sudo openssl rsa -in server.key.password -out server.key
   Enter pass phrase for server.key.password:
   writing RSA key
   $

Now generate certificate request with the rsa key file

.. code-block:: console

   $ sudo openssl req -out server.csr -new -key server.key
   You are about to be asked to enter information that will be incorporated
   into your certificate request.
   What you are about to enter is what is called a Distinguished Name or a DN.
   There are quite a few fields but you can leave some blank
   For some fields there will be a default value,
   If you enter '.', the field will be left blank.
   -----
   Country Name (2 letter code) [AU]:IN
   State or Province Name (full name) [Some-State]:TamilNadu
   Locality Name (eg, city) []:Chennai
   Organization Name (eg, company) [Internet Widgits Pty Ltd]:DrunkenMonk Private Limited
   Organizational Unit Name (eg, section) []:Pattasarayam Generating Unit
   Common Name (e.g. server FQDN or YOUR name) []:drunkenmonk.org
   Email Address []:webmaster@drunkenmonk.org

   Please enter the following 'extra' attributes
   to be sent with your certificate request
   A challenge password []:
   An optional company name []:
   $

We should not give password when creating certificate request. Ok, know time to create self signed certificate.

.. code-block:: console

   $ sudo openssl x509 -req -in server.csr -out server.crt -signkey server.key -days 365
   Signature ok
   subject=/C=IN/ST=TamilNadu/L=Chennai/O=DrunkenMonk Private Limited/OU=Pattasarayam Generating Unit/CN=drunkenmonk.org/emailAddress=webmaster@drunkenmonk.org
   Getting Private key
   $

Now we have two files ``/etc/ssl/dovecot/server.key`` and ``/etc/ssl/dovecot/server.crt`` to use it for SSL. Lets configure dovecot now, we need to modify ``/etc/dovecot/dovecot.conf`` like this,

::

   protocols = imap pop3
   listen = *, ::

Configuration not ends with ``/etc/dovecot/dovecot.conf``, it has different conf files for different purpose inside ``/etc/dovecot/conf.d/``, lets modify one by one, Here is ``/etc/dovecot/conf.d/10-auth.conf``

::

   auth_mechanisms = plain login
   !include auth-system.conf.ext
   #!include auth-sql.conf.ext
   #!include auth-ldap.conf.ext
   #!include auth-passwdfile.conf.ext
   #!include auth-checkpassword.conf.ext
   #!include auth-vpopmail.conf.ext
   #!include auth-static.conf.ext

If you want to permit only current machine's users and don't want to use ldap or other machinisms, then make sure you comment all includes except ``auth-system.conf.txt``.

Here is modifications inside ``/etc/dovecot/conf.d/10-logging.conf``

::

   log_path = syslog
   syslog_facility = mail

Here is modifications inside ``/etc/dovecot/conf.d/10-mail.conf``

::

   mail_location = mbox:~/.mail:INBOX=/var/mail/%u

Now ``/etc/dovecot/conf.d/10-ssl.conf``

::

   ssl = yes
   ssl_cert = /etc/ssl/dovecot/server.crt
   ssl_key = /etc/ssl/dovecot/server.key

Thats all from config stuff, lets restart dovecot.

.. code-block:: console

   $ sudo eselect rc restart dovecot

Now, we need to test if it is working, We need to connect with dovecot through imap port with TLS encryption

.. code-block:: console

   $ openssl s_client -connect localhost:143 -starttls imap

Above command will show lot of SSL stuffs, and then finally dovecot will say ``. OK``, we need to start communication with dovecot from there,

::

   . OK Pre-login capabilities listed, post-login capabilities have more.
   a login mokka somepassword
   * CAPABILITY IMAP4rev1 LITERAL+ SASL-IR LOGIN-REFERRALS ID ENABLE IDLE SORT SORT=DISPLAY THREAD=REFERENCES THREAD=REFS MULTIAPPEND UNSELECT CHILDREN NAMESPACE UIDPLUS LIST-EXTENDED I18NLEVEL=1 CONDSTORE QRESYNC ESEARCH ESORT SEARCHRES WITHIN CONTEXT=SEARCH LIST-STATUS SPECIAL-USE
   a OK Logged in
   b select inbox                   
   * FLAGS (\Answered \Flagged \Deleted \Seen \Draft)
   * OK [PERMANENTFLAGS (\Answered \Flagged \Deleted \Seen \Draft \*)] Flags permitted.
   * 0 EXISTS
   * 0 RECENT
   * OK [UIDVALIDITY 1343790396] UIDs valid
   * OK [UIDNEXT 320] Predicted next UID
   * OK [NOMODSEQ] No permanent modsequences
   b OK [READ-WRITE] Select completed.
   c list "" *
   * LIST (\HasNoChildren) "/" "INBOX"
   c OK List completed.

The above session with dovecot shows that things are ok. You can now configure your ``Thunderbird`` or ``Evolution`` to use your machine for local emails. Have a nice day!!
