.. title:: Dnsmasq for Home User

.. _dnsmasq-for-home-user:

*********************
Dnsmasq for Home User
*********************

| Updated: |modifieddate|

Every BSNL broadband subscribers know that BSNL's default nameservers are one of the worst in response times. Even if you have connectivity exceeding 1gbps, If your nameservers are not good, then your internet experience will not be good.

So, I decided to use google's nameservers. I could have configure NetworkManager not to get nameservers through DHCP instead ask it to use google's nameservers, but I thought of giving ``dnamasq`` a try.

.. image:: /_static/images/2012/08/dnsmasq_icon.jpg
   :target: http://www.thekelleys.org.uk/dnsmasq/doc.html
   :align: center

For those who don't know what is mean by `dnsmasq <http://www.thekelleys.org.uk/dnsmasq/doc.html>`_, it is a dns forwarder which can handle DNS, DHCP and BOOTP requests. It means, it can act like `bind <http://www.isc.org/software/bind>`_, `dhcpcd <http://www.isc.org/products/DHCP>`_ and `tftpd <http://en.wikipedia.org/wiki/Trivial_File_Transfer_Protocol>`_.

Configuring dnsmasq is pretty simple and straight forward. We have to modify two files ``/etc/hosts`` and ``/etc/dnsmasq.conf``. Here is my ``/etc/hosts`` file

::

   127.0.0.1     mokka.drunkenmonk.org mokka localhost
   ::1           mokka.drunkenmonk.org mokka localhost
   192.168.2.1   bridge.mokka.drunkenmonk.org

The first line says ``127.0.0.1`` is the ip address for mokka.drunkenmonk.org and also for ``mokka`` and ``localhost`` hostnames. I other words, ``mokka``, ``localhost`` and ``mokka.drunkenmonk.org`` will resolve to 127.0.0.1 ip address. 

``mokka.drunkenmonk.org`` is a cononical name which contains two parts, ``mokka`` as hostname and ``drunkenmonk.org`` as domainname. So, by putting ``drunkenmonk.org``, I'm also setting domainname for my system.

I really don't know the proper way to configure domainname in my sabayon (in other words gentoo). From googling, I came to know that putting ``domain drunkenmonk.org`` into ``/etc/resolv.conf`` is the right way to set domainname in a linux system. But, ``/etc/resolv.conf`` is such dynamic nowadays, we can't be sure who will modify it later and I never see a router providing domainname in DHCP response (Not in BSNL's ADSL routers for sure). So I endup putting my domainname in ``/etc/hosts`` file.

The second line is same like the first one, but for ipv6.

The third line says ``192.168.2.1`` is the ip address for ``bridge.mokka.drunkenmonk.org``. I'll come to this later. Now, take a look at my /etc/dnsmasq.conf

.. code-block:: bash

   domain-needed
   bogus-priv
   no-resolv
   server=8.8.8.8
   server=8.8.4.4
   local=/drunkenmonk.org/
   domain=drunkenmonk.org
   dhcp-range=interface:br0,192.168.2.2,192.168.2.254,255.255.255.0,1d
   mx-host=drunkenmonk.org,mail.drunkenmonk.org,30
   cname=drunkenmonk.org,bridge.mokka.drunkenmonk.org
   cname=www.drunkenmonk.org,bridge.mokka.drunkenmonk.org
   cname=mail.drunkenmonk.org,bridge.mokka.drunkenmonk.org

``domain-needed`` instructs dnsmasq to never forward DNS queries which don't have domain part. It means, If you query for ``nslookup google`` dnsmasq will never forward it to upstream server (In my case, to google's nameservers).

``bogus-priv`` tells dnsmasq never forward reverse-lookup queries which have local subnet's ip rage to upstream. Which means ``nslookup 10.0.0.1`` will not be forwarded to upstream instead dnsmasq will try to resolv itself. If it doesn't find hostname for 10.0.0.1 in /etc/hosts or its dhcp leases, then it will send back ``no such domain`` response.

``no-resolv`` says dnsmasq will not read /etc/resolv.conf to get upsteram nameservers. Normally dnsmasq will read /etc/resolv.conf file to get upstream nameservers.

``server=8.8.8.8`` and ``server=8.8.4.4`` instucts dnsmasq to use google's 8.8.8.8 and 8.8.4.4 as primary and secondary DNS nameservers (or in other words upstream nameservers).

``local=/drunkenmonk.org/`` says that DNS queries with hostnames like ``mokka.drunkenmonk.org`` should not be forwarded to upstream intead dnsmasq should resolve it from /etc/hosts or from its dhcp leases.

``domain=drunkenmonk.org`` tells dnsmasq to send ``domain=drunkenmonk.org`` in DHCP response so that machines configured through DHCP will come under ``drunkenmonk.org`` domain.

``dhcp-range=interface:br0,192.168.2.2,192.168.2.254,255.255.255.0,1d`` instructs dhsmasq to allocate ip address between 192.168.2.2 and 192.168.2.254 with netmask 255.255.255.0 for 1 day only to the DHCP requests coming from br0 interface. Which means, machines connected through br0 interface and asking for DHCP response will get ip address between 192.168.2.2 and 192.168.2.254 for 1 day. dnsmasq will not send response to requests coming from interfaces other than br0.

``mx-host=drunkenmonk.org,mail.drunkenmonk.org,30`` tells dnsmasq to send ``mail.drunkenmonk.org`` as MX response for ``drunkenmonk.org`` domain. Which means, when you do ``nslookup -q=MX drunkenmonk.org`` it will give ``mail.drunkenmonk.org`` as response.

``cname`` lines are like aliases. means when you do ``nslookup www.drunkenmonk.org`` it will resolve to ``bridge.mokka.drunkenmonk.org`` which resolves to ``192.168.2.1`` according to /etc/hosts file. So dnsmasq will send ``192.168.2.1`` as response.

Thats all from configuration stuff. These changes will take effect once you restart dnsmasq service. But this does not means that all your DNS queries will go through your machine's dnsmasq daemon, If you have nameserver entry in ``/etc/resolv.conf``, your machine's DNS queries will go to ``/etc/resolv.conf`` nameservers. So, there is no use of using dnsmasq. You have to make sure you are not creating entries in ``/etc/resolv.conf``. If you are a home user, make sure you select ``Automatic (DHCP) Address only`` in ``Method`` drop-down list inside ``ipv4 settings`` tab in NetworkManger.

You can check that you are getting response from dhsmasq with below commands,

.. code-block:: console

   $ dig ANY drunkenmonk.org
   ; <> DiG 9.9.1-P2 <> ANY drunkenmonk.org
   ;; global options: +cmd
   ;; Got answer:
   ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 39480
   ;; flags: qr aa rd ra ad; QUERY: 1, ANSWER: 2, AUTHORITY: 0, ADDITIONAL: 0
   ;; QUESTION SECTION:
   ;drunkenmonk.org.               IN      ANY
   ;; ANSWER SECTION:
   drunkenmonk.org.        0       IN      CNAME   bridge.mokka.drunkenmonk.org.
   bridge.mokka.drunkenmonk.org. 0 IN A    192.168.2.1
   ;; Query time: 0 msec
   ;; SERVER: 127.0.0.1#53(127.0.0.1)
   ;; WHEN: Mon Aug  6 00:15:16 2012
   ;; MSG SIZE  rcvd: 99

The line contains ``SERVER:`` tell us that our DNS query is served by ``127.0.0.1``, means our local machine. Here is the command to verify MX record,

.. code-block:: console

   $ dig MX drunkenmonk.org
   ; <> DiG 9.9.1-P2 <> MX drunkenmonk.org
   ;; global options: +cmd
   ;; Got answer:
   ;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 14301
   ;; flags: qr aa rd ra ad; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 0
   ;; QUESTION SECTION:
   ;drunkenmonk.org.               IN      MX
   ;; ANSWER SECTION:
   drunkenmonk.org.        0       IN      MX      30 mail.drunkenmonk.org.
   ;; Query time: 0 msec
   ;; SERVER: 127.0.0.1#53(127.0.0.1)
   ;; WHEN: Mon Aug  6 00:16:38 2012
   ;; MSG SIZE  rcvd: 71

If you are not introduced to `Linux's bridge interface <http://www.lainoox.com/tag/linux-bridge-howto>`_, you may wonder what the hell is ``br0``. Well , it is a virtual device in Linux which act like a physical network device (like eth0). I have particularly configured my dnsmasq to serve DHCP requests to whoever connect through my bridge interface. Howto use this ``br0`` effectively to configure networking in my Qemu virtual machines is going to be my next :ref:`virtual-home-network` article. Have a nice day!!
