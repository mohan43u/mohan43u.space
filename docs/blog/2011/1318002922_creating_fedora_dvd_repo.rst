.. title:: Creating Fedora DVD Repo

************************
Creating Fedora DVD Repo
************************

| Updated: |modifieddate|

Hi,

Its almost 1 year since my last post, what happens to me? It a 1 year story to say. But in short, my laptop became older and eventually went down. Graphics card gone. It took a while for me to buy another new one. Here I am.

In between lot thing happened, Something I just wanted to post, but now I can't remember them. As a fresh start, This is what happened when I got my new ``Samsung Notebook``. Ordered through FlipKart and got it by last week.

I know that I can't get Internet because I cancelled my netconnect+ subscription way back when my previous lappy started giving troubles. Thus, My beloved debian is out of reach. So, I decided to get Fedora or Mint DVD from someone and Install it. Thanks to Srini, Amachu and his Gang, for providing Fedora DVD to me. Installation went smoothly and for the first time, I'm in Gnome3,

.. figure:: /_static/images/2011/10/screenshot.png

   Fedora 15 Desktop

Installation went smoothly and to my surprise, no more tinkering on mobile modem. Thanks to those guys who created profile for Aircel. At first, bluetooth daemon didn't start, googled and fixed it.

While playing with Gnome3, I thought of using installation DVD as my repo, because I have limited network with very little speed, It would be better to use existing DVD as my repo. So, that starts my hunger and this is how I ended.

1. Inserted my DVD and monitored how my DVD is mounting. Seems ``org.freedesktop.UDisks`` takes care of mounting and it always mount my DVD inside ``/media`` folder taking mount directory name from ``ENV{ID_FS_LABEL_ENC}`` Udev environment variable. I came to know that yum is facing some trouble If I have a ``fedora-dvd.repo`` file like below under ``/etc/yum/yum.repos.d`` directory,

.. code-block:: ini

   [fedora-dvd]
   name=Fedora DVD $releasever - $basearch
   baseurl=file:///media/Fedora 15 i386 DVD
   enabled=1
   gpgcheck=1
   gpgkey=file:///media/Fedora 15 i386 DVD/RPM-GPG-KEY-fedora-$basearch

So, it seems yum cannot handle white-space in URI. Then, I decided to write a Udev rule to rename ``Fedora 15 i386 DVD`` to ``Fedora15i386DVD``, So that ``org.freedesktop.UDisks`` will mount it as ``file:///media/Fedora15i386DVD``. Here is the rule file

.. code-block:: console

   $ cat /etc/udev/rules.d/99-rename-fedora-dvd.rules
   ENV{ID_FS_LABEL_ENC}=="Fedora\x2015\x20i386\x20DVD", ENV{ID_FS_LABEL_ENC}="Fedora15i386DVD"
   $

Then, I recreated fedora-dvd.repo as below

.. code-block:: console

   $ cat /etc/yum.repos.d/fedora-dvd.repo
   [fedora-dvd]
   name=Fedora DVD $releasever - $basearch
   baseurl=file:///media/Fedora15i386DVD
   enabled=1
   gpgcheck=1
   gpgkey=file:///media/Fedora15i386DVD/RPM-GPG-KEY-fedora-$basearch
   $

One of the main reason to add this ``Fedora15i386DVD`` repo is to install development tools without internet. In a way it is more useful than a plain LiveCD.

Don't forget to insert Fedora DVD If you follow this setup and want to install something from Fedora DVD. A little google tells me that there are lot of ways to achieve this DVD-to-Repo thing in Fedora, I'm relatively new to Fedora, I will learn as I move on.
