.. title:: Idea

.. raw:: latex

   \newpage

=========================================
How we came up with the idea about MeeTTY
=========================================

| Updated: |modifieddate|


Pandemic
--------

Apr 2020, Its the start of `COVID-19 pandemic`_, governments all around the world started lock-down, no one should roam around outside their houses. I'm one of the coordinator of `ilugc.in`_ who conducts monthly meetings every 2 :superscript:`nd` Saturday. Last month meet (Mar, 2020) was conducted physically at `IIT`_. It is not possible to conduct this month meet physically. We have to go online.


Frustration
-----------

This month's meet is due in next week, obviously we are going online this month like every other special interest groups around the world. But, we are yet to decide on how we are going to conduct the online meet. As our group only promotes `Free and Open Source software`_, we cannot go with online meeting platforms like `Zoom`_ or `Google Meet`_ or some other commercial platforms. We have to stick with FOSS based solutions. These are the choices we considered at that time,

`Jitsi`_

   Just like most FOSS group around us, we would have went with Jitsi, but somehow we didn't want to go with jitsi. It may be because we thought of installing Jitsi on our own server to improve video playback but ended up not choosing it because of the thought of buying/creating new instance in aws and doing the actual installation. At this time, we were looking for a solution which don't involve anything to setup in a server.

`Jami`_

   Jami looked very interesting, its peer to peer and no server involved [#]_. But when we tried it in different OSes, the application crashed in OSX, Ubuntu LTS, it only somehow stable in Arch. Also functionalities are not consistent, and some functionalities didn't work. So we decided not to use at this time.

`Big Blue Button`_

   At least for Jitsi, we can use it without doing any server setup through https://meet.jit.si/ , but for BBB, we have to setup server. Also, it doesn't support `latest distros`_. So, BBB is out of our choice.

`Matrix.org`_

   Matrix can do video conferencing using jitsi bridge. Again, this also involved setting up a server with jitsi and matrix and connecting each other. So, we didn't choose Matrix.

   All the platforms we considered are proper audio/video based conference solutions. They need proper servers with enough bandwidth to operate in a useful way. We lacked a server and not ready to invest.


The Trigger
-----------

We were all discussing and evaluating the platforms at `#ilugc`_ [#]_ channel in `Freenode`_. At that time, `shrini`_ asked this question,

   "Why do we need full audio/video platforms, why can't we just use IRC and terminals?"

   -- shrini

We started reasoning this question, like what we loose if we go with IRC based communication instead of live video conference. We already know that IRC works for communication, its text based and doesn't suck bandwidth. The meetings we conduct are mostly like someone will talk about a technical topic which involves showing terminal commands and explain about what those commands do to achieve specific goals (like setting up `nginx`_ etc.,) so IRC is enough to convey the message to audiance.

Now, as I previously said, most of our ilugc talks involves showing commands and explaining what they do. For the explanation part, we already arrived at the conclusion that IRC is enough. Now, we need to find a way for presenter to share terminal to the outside word.

.. topic:: The Idea

   This is where things got interested, I already know that there are ways to show terminal output in a webpage [#]_. I also know that python can do `pty`_, means, it can create a terminal, run a program on that created terminal and handle input/output of that created terminal which in turn will handle input/output to that program.

  So I thought why not just combine python's pty with whatever `solution`__ exists to show terminal output in web?

I come up with this concept,

* run a server to serve a webpage
* the webpage should contain two parts which uses `xterm.js`__ to show terminal output
* the first part should show the terminal of presenter, it should be read-only, whatever the presenter do in the server should be displayed in this presenter's terminal.
* the second part should show another terminal to practice what the presenter is explaining. This terminal should also be capable of getting input from user

I quickly started prototyping my idea, I used the below tools

`flask`_,  `flask-socketio`_ and `socket.io`_

   Flask is used to serve webpage, flask-socketio is used to handle inputs from `xterm.js`__, whenever a new page get served, socket.io will immediately initiate two websocket back to server, flask-socketio will handle those connection, one websocket is for ``presenter's terminal`` called ``tmux`` and another websocket is for ``practice terminal`` called ``nspawn``.

   Whenever ``tmux`` websocket connection comes in, flask-socketio will create a new pty and will start :envvar:`tmuxcmdline` defined in the environment. The input and output of that commandline will be sent back and forth through ``tmux`` websocket so that it will appear in the ``presenter's terminal`` part of the webpage
 
   Whenever ``nspawn`` websocket connection comes in, flask-socketio will create a new pty and will start :envvar:`nspawncmdline` defined in the environment. The input and output will be handled through ``nspawn`` websocket so that it will appear in ``practice terminal`` part of the webpage

.. _tmux-anchor:

`tmux`_

   tmux is a fantastic screen multiplexer. It uses client-server model, for our purpose we basically start tmux client through :envvar:`tmuxcmdline` to wait and connect to a particular ``tmux session`` called ``broadcast`` in read-only mode. So all the ``presenter's terminal`` created through MeeTTY's webpage will be read-only clients to a ``broadcast`` tmux session.

   If we want to show something to the audiance, all we need is to connect to the same ``broadcast`` tmux session as read-write client and start typing, everything we type will be replicated automatically to all the read-only clients which are connected to the same ``broadcast`` tmux session. Thus, we achieve the goal of broadcasting presenter's terminal to multiple audiance.

`linux usernamespace`_

   Opening a terminal in a webpage and expose it to the whole world is the stupidest craziest idea. But, the thought of providing a place for audiance to practice what the presenter is teaching is very interesting. So, instead of starting a direct shell, we utilize linux usernamespace container to start a shell through :envvar:`nspawncmdline` and attach it to ``practice terminal`` through ``nspawn`` websocket. Thus, ``practice terminal`` will have a restricted shell which audiance can execute commands.

`kiwiirc`_

   I completed my prototype and showed it to people in #ilugc, People provided great feedback, shrini come up with the idea to include a web IRC client called kiwiirc so that people can use a single web page to login into #ilugc channel, see what the presenter is teaching through commands and practice those commands. `mbuf`_ suggested to have single page and show all three parts of the webpage (``IRC window``, ``Presenter's Terminal`` and ``Practice Terminal``) in a single page instead of having three tabs to switch between them. He also suggested to put ``Powered By`` label somewhere in the webpage. I didn't realize the significance of putting that label, but after sometime I understood how important that label is, The word ``Attribution`` in Open Source licenses refers to this label.

.. centered:: *So, this is how we establish two terminals and an IRC client in MeeTTY's webpage.*

In the mean time, April month meet took place through #ilugc channel. But we didn't introduce MeeTTY to ILUGC monthly meet participants. At this point, you may think that we almost reached our goals, but this is just a start.

|

.. container:: rightalign

   To be continued..

|

.. envvar:: tmuxcmdline

   This environment variable contains the commandline which gets executed whenever there is new ``tmux`` websocket establishes. This will connect to the ``broadcast`` tmux session as read-only client.

.. envvar:: nspawncmdline

   This environment variable contains the commandline which gets executed whenever there is new ``nspawn`` websocket establishes. This will start new shell under linux usernamespace.

|

.. [#] actually TURN is needed, but its another story
.. [#] we reactivated and acquired admin rights to this channel after the start of the pandemic when we noticed that #ilugc channel was abandoned long before
.. [#] I don't know exactly `how to do it`_, at this time, but learnt later.

__ `how to do it`_
__ `how to do it`_
__ `how to do it`_

|

.. target-notes::

.. _covid-19 pandemic: https://en.wikipedia.org/wiki/Coronavirus_disease_2019
.. _ilugc.in: https://ilugc.in
.. _iit: https://www.iitm.ac.in
.. _free and open source software: https://en.wikipedia.org/wiki/Free_and_open-source_software
.. _zoom: https://zoom.us
.. _google meet: https://meet.google.com
.. _jitsi: https://jitsi.org
.. _jami: https://jami.net
.. _big blue button: https://bigbluebutton.org/
.. _latest distros: https://github.com/bigbluebutton/bbb-install/issues/37
.. _matrix.org: https://matrix.org
.. _#ilugc: https://meet.ilugc.in
.. _freenode: https://freenode.net
.. _shrini: https://goinggnu.wordpress.com
.. _mbuf: http://shakthimaan.com/
.. _nginx: https://nginx.org
.. _how to do it: https://xtermjs.org
.. _pty: https://docs.python.org/3/library/pty.html
.. _flask: https://flask.palletsprojects.com/en/1.1.x/
.. _flask-socketio: https://flask-socketio.readthedocs.io/en/latest/
.. _socket.io: https://socket.io/
.. _tmux: https://github.com/tmux/tmux/wiki
.. _linux usernamespace: https://man7.org/linux/man-pages/man7/user_namespaces.7.html
.. _kiwiirc: https://kiwiirc.com/
