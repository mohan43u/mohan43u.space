.. title:: reStructuredText

.. _playing-with-restructuredtext:

*****************************
Playing with reStructuredText
*****************************

| Updated: |modifieddate|

This is a paragraph. This is an example to *italics*, this is an example to ``bold``, this is an example to `passthrough`, this is an example to ``literal text``, this is an example to single word hyper link google_, this is an example to `same google but with space between`_, this is also an example example to google link__, this is also an example to `google link with space between`__

this is an example to footnote [CIT01]_ reference, footnote reference can be [#]_ auto [#]_ numbered, this is an example to https://reddit.com url. Something need to be changed.

Example bullet list,

* one
* two

   * one one
   * one two

1. one
2. two

   1. one one
   2. one two

This is also numbered list,

#. one
#. two

   #. one one
   #. one two

Term 1
   Definition 1
Term 2
   Definition 2

This is a normal paragraph
without broken lines

| this paragraph
| contains broken lines

this is normal paragraph introducing field definition

:variable0: variable0 definition
:variable1: variable1 definition

this is normal paragraph introducing option definition

-a	short option
--analog	long option

::

   this is literal block
   anything written here will show
   differently

this is normal paragraph to show shortand liternal block beginning :: 

   this is liternal block
   with broken lines

this is example for quoted block ::

> this line is not intended
> and also lines can be broken
> this block begins with ">" character

this is example to line blocks ::

| this line is
| a line block

this is example to block quote

   "this is a quote"
 
   -- Mohan R

.. highlight:: c

this is normal paragraph introducing c code highlighting through ``highlight::`` directive ::

   #include <stdio.h>
   int main(int argc, char* argv[]) {
	printf("hello world\n");
	return 0;
   }

.. highlight:: bash

this is normal paragraph introducing bash code highlighting through ``highlight::`` directive ::

   $ echo "this is import bash" or is this python?

.. highlight:: python

this is normal paragraph introducing python highlighting through ``highlight::`` directive ::

   import os
   import sys
   def hello():
	print("hello world")

this is a normal paragraph introducing c highlighting through ``code-block::`` directive

.. code-block:: c
   :linenos:

   #include <stdio.h>
   int main(int argc, char* argv[]) {
	printf("hello world\n");
	return 0;
   }

this is a normal paragraph introducing console highlighting through ``code-block::`` directive

.. code-block:: console

   $ echo "is this import bash" or is this python?

this is a normal paragraph introducing python highlighting through ``code-block::`` directive

.. code-block:: python
   :linenos:

   import os
   import sys
   def hello():
	print("hello world")

this is a normal paragraph introducing ``literalinclude::`` directive

.. literalinclude:: /_static/misc/helloworld.c
   :language: c
   :linenos:

this is a normal paragraph introducing same ``literalinclude::`` directive with specific lines set as ``:lines: 3-5``, ``:lines:8`` and ``lines:1-``

.. literalinclude:: /_static/misc/helloworld.c
   :language: c
   :lines: 3-5

.. literalinclude:: /_static/misc/helloworld.c
   :language: c
   :lines: 8

.. literalinclude:: /_static/misc/helloworld.c
   :language: c
   :lines: 1-

this is normal paragraph introducing doctest

>>> 1 + 1
2

this is a normal paragraph introducing table

+-------+-------+
| head1 | head2 |
+=======+=======+
| col1  | col2  |
+-------+-------+

this is a paragraph introducing inline `link to python <https://python.org>`_.

.. _my-ref:

this is one internal section
============================

this paragraph contains :ref:`my-ref` link to the section it contains.

this is a paragraph this contains inline roles like :emphasis:`italic`, :strong:`bold text`, :literal:`literal text`, subscript :subscript:`text`, superscript :superscript:`text`

.. attention::

   this is an attention text

.. caution::

   this is caution text

.. danger::

   this is danger text

.. error::

   this is error text

.. hint::

   this is hint text

.. important::

   this is important text

.. note::

   this is note text

.. tip::

   this is tip text

.. warning::

   this is warning text

.. admonition:: Generic Admonition
   :class: generic-class
   :name: generic-name

   this is admonition text

.. centered:: This is a centered text

.. hlist::
   :columns: 4

   * one
   * two
   * three
   * four

this is a normal paragraph introducing image

.. image:: /_static/images/test.jpg
   :width: 150
   :height: 150
   :scale: 300
   :align: center

.. topic:: Topic

 this is a topic which is not attached to document flow

.. sidebar:: Sidebar
   :subtitle: Sidebar Subtitle

   this is a sidebar with subtitle

.. epigraph::

  some stupid shit called ephigraph

  -- Mohan R

.. table:: Table with Title
   :widths: auto

   +-------+-------+
   | head1 | head2 |
   +=======+=======+
   | col1  | col2  |
   +-------+-------+

.. csv-table:: CSV Table
   :widths: auto
   :header-rows: 1

   head1, head2
   col1, col2

.. list-table:: List Table
   :widths: auto
   :header-rows: 1

   * - head1
     - head2
   * - col1
     - col2

.. this is a comment

|

__ google_
__ `same google but with space between`_
.. [CIT01] example reference
.. [#] this is first reference
.. [#] this is second reference

.. target-notes::

.. _google: https://google.com
.. _`same google but with space between`: https://google.co.in
