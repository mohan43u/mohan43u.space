.. title:: Milana (2007) - From a Tamil Fan

********************************
Milana (2007) - From a Tamil Fan
********************************

| Updated: |modifieddate|

Declaimer: By no means this is a review. This is how I came to know about this movie and enjoyed.

One of my best friend (he is from mysore) had that song as his ringtone at that time when milana was released. Its so catchy, that whenever I was bored, I would go to his desk and start listening to that song. Yes, I'm talking about ``Ninnindale..``

Now, I'm in different place and my friend is in different place. recently I spoke with him, suddenly my mind ( What!!!??? isn't my heart?? ) thought just get that song and have it in my mobile. Also I saw lot of positive reviews and massive statistics (100 days.. 200.. days in Bengaluru PVR.. don't know the exact statistics mate!!). Even though my relatives are in Bengaluru, I didn't get a chance to watch a kannada movie. This is the one which I watched for the first time.

His sharp face and uncombed hair ( Is it his trademark appearance? ) first gave some different opinion about Puneet Rajkumar, But his performance as Akash, showed how good he is, as a Actor. Be it as a Son, a Mirchi RJ, Husband, Friend, Fighter, Dancer everything, shows the capabilities of a mass hero( is he already? I see he is called ``Power Star``, but nowadays every star have a subname!!). I hope he should not develop any style pattern like many stars in south. He should act for directors.

Like Akash, his wife Anjali (Parvathi menon) who ask divorce in her first night, steal some important scenes as a good Actress (Yaepulla ``Maari``, neeiya idhu, romba alaga iruka pulla). Her cute face expressions especially when she get angry because of akash's Bred & Jam flirt, indicates a good chemistry between both ( is there any gosip in KFI about them? I came to know that they had another big hit, Prithvi..). Later at the end, when she suffer because ``Akash`` didn't realize her love,  makes a real beautiful love story.

On the whole, This movie  shows how good a family, friendship and love can bring happiness to a person's life. Really!! Its a great movie to watch. Mano Moorthy's Songs and Background Music will capture anyone's heart!! I enjoyed every frame!!! Thanks to Director Prakash and the whole team for this wonderful movie.

Somebody pleeeasssseeee!!!!! bring this movie to Kollywood!!!!
