.. title:: Support for nVidia GeForce graphics card - Debian - Testing

***********************************************************
Support for nVidia GeForce graphics card - Debian - Testing
***********************************************************

| Updated: |modifieddate|

As you can see in my previous article, this is about getting my nVidia card to work with Debian.

By default Xorg provides ``nv`` driver. Its unfortunate that nVidia not even working with Xguys for basic compatibility for ``nv`` driver. As a result, the default ``nv`` driver is not usable in my machine.

I'm not left alone here. Debian have a package called ``nvidia-glx`` which will install full support for nvidia cards with proprietary ``nvidia`` driver.

.. code-block:: console

   $ su -c'aptitude install nvidia-glx'

But currently, this is not working in Debian Testing. `I got a bug, last x11 update breaks things with nvidia-glx <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=549869>`_.

Now also I'm not left alone, using ``module-assistant`` I can still get the nvidia driver.

.. code-block:: console

   $ su -c'aptitude install module-assistant'
   $ su -c'module-assistant prepare'
   $ su -c'module-assistant'

After the above steps I will get ncurses interface for module-assistant, choose ``nvidia kernel`` in the wizard and compile the nvidia-kernel package. If everything goes right, I will get ``nvidia`` driver in ``lib/modules/$(uname -r)/kernel/drivers/video/nvidia.ko``. Also my ``/etc/x11/xorg.conf`` file will be modified to use ``nvidia`` driver. Thats all, I can now start X session.

Unfortunately, this method is also not working currently, `while compiling I'm getting this error <http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=548840>`_.

So there is no other way for me then to go for nvidia's own builder. You can download the driver+installer `from this location. <http://www.nvidia.com/object/unix.html>`_. Now we need to make our system capable for compilation.

.. code-block:: console

   $ su -c'aptitude install module-assistant'
   $ su -c'module-assistant prepare'

Then, the next step is just run the nvidia's installer

.. code-block:: console

   $ su -c'chmod 755 NVIDIA-Linux-x86_64-190.53-pkg2.run'
   $ su -c'./NVIDIA-Linux-x86_64-190.53-pkg2.run'
   $ su -c'aptitude install nvidia-kernel-common'
   $ su -c'depmod -aeF /boot/System.map-$(uname -r)'
   $ su -c'update-initramfs -u'

The above step will automatically compile and install the ``nvidia.ko`` driver in ``/lib/modules/$(uname -r)/kernel/drivers/video/nvidia.ko``. Also my ``/etc/x11/xorg.conf`` file will be modified to use ``nvidia`` driver. Thats all, I can now start X session.
