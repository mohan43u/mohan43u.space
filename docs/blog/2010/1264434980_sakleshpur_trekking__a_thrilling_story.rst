.. title:: Sakleshpur Trekking - A thrilling story

***************************************
Sakleshpur Trekking - A thrilling story
***************************************

| Updated: |modifieddate|

Aah!! at last I got some time to blog, I was stuck up nearly two month doing RHCE and i'm happy that I did it in first shot itself. In the mean time, one of unforgettable moment happened in my life.

Dec 17, A serious discussion happened in my team, everybody want to go to their home inviting team members as their guest saying that there are nice places to go nearby. I also invited colleagues to vellore (What?? are you cracy?? - I know... I know.. a big laugh around my teammates..). Srinath surfed like anything to find a new, unpopular, best place in karnataka. Finally he saw a blog about ``Sakleshpur``. Nobody knows this untill that night.

Dec 18, We decided to go wherever in karnataka, on that night, srinath arranged a car, took lot of maps routing to sakleshpur from chennai, everyone finished their work, at 11.30, I started my journey with srinath. After we reached our office, we took manju, karthik, saravanan and sridhar heading to sakleshpur.

.. figure:: /_static/images/2010/01/img_1285.jpg

   Karthk, Saravanan, Srinath, Sridhar and Manju

Dec 19, 6:00 am, We are at banglore - tumkur outer ring road, wow!! top class roads!!

.. figure:: /_static/images/2010/01/img_1292.jpg

   Banglore - Tumkur outer ring road

Dec 19, 11.00 am, our first stop for morning breakfast, moving towards hassan,

.. figure:: /_static/images/2010/01/img_1312.jpg

   First stop for breakfast with this beautiful baby!!

Dec 19, 12.00 pm, our second stop, just to see a village which impressed saravanan, 

.. figure:: /_static/images/2010/01/dsc05443.jpg

   villages - True India !!

Dec 19, 2.00 pm, we are at outer side of hassan, moving towards sakleshpur, one of the best journey in my life, full of greenery. 

.. figure:: /_static/images/2010/01/dsc05495.jpg

   Hassan to Sakleshpur

Dec 19, 3.00 pm, we reached sakleshpur, booked a room, everyone want to take a nap, but no time for us, heading to the ``Green Track`` immediately. Till now we don't know what kind of place that is.

We took auto from sakleshpur to dhonigal, after giving money to that driver, he advised to be careful, trains will come at any time and you will not have space to stand aside every-time. Also he asked us to watch out for elephants, jaguars and forest beasts, because previously two men went inside this deep forest and came back as skeletons !!!!!!!!!!!!!!! :0

After this initial fear, we started our treking, its a small mountain climb, we reached the track, its ``Dhonigal`` railway station, darkness taking raise !!

.. figure:: /_static/images/2010/01/dsc05514.jpg

   Donigal railway station

Even though its risky, we had no other choice now, heading towards ``Yedakumeri`` railway station. Fully covered by trees, everywhere is green, just amazing!! and thrilling !!

.. figure:: /_static/images/2010/01/dsc05543.jpg

   no wonder why its called green track!!

Dec 19, 6.00 pm, its almost dark, we heared some kind of sound, its a train!! heading towards us, here we got a place to step aside and captured that train in our cameras

.. figure:: /_static/images/2010/01/dsc05548.jpg

   First train !!

Dec 19, 6.30 pm, its fully dark, we stuck in a middle of deep forest, everybody got some kind of uncertainty and discussed about dropping the plan and back to sakleshpur, but there is no choice for us. Even going back to sakeleshpur is as dangerous as going to ``Yedakumeri``. 

At that time, we saw someone coming towards us. Manju asked that person ``who are you?``, because he only know kannada, with initial fearness solved, that person is the one god sent to save us. It seems he is heading towards his home near dhonigal railway station, and scolded us for unplanned journey. He called his friend who is staying in one of the railway workers quarters and explained the situation. His friend is also helpful that he promissed to come here and take us to his home, also asked this person to stay untill he come.

As he said, his friend came and took us to his home. Near his home, there is another abandoned home, he broked those locks and gave it to us saying ``If somebody asks!! I will tell that its thiefs work!! they broken this locks``. We thanked him a lot. at last our fears came to somewhat minimal.

.. figure:: /_static/images/2010/01/dsc05567.jpg

   Walking in a railway bridge!!

.. figure:: /_static/images/2010/01/dsc05573.jpg

   Savior!!!

Dec 19, 7.30 pm, sridhar's airtel got some thin signal, everybody started to call and inform to their beloved ones, I took it and sent a message to kesavan ``Machi, mohan here, I'm in middle of deep forest situated between dhonigal - yedakumari, If i'm not calling to you before morning 10.00 tomorrow, please inform to police, also tell to my father, please dont panic, don't tell it now to my father, I'm safe here!!``.  Kesav behaved as I said.

After this, everyone got some relax, talked freely and cracked jokes !! thinks came normal !! and we started a camp fire outside that home. Its full of dark we don't know how much beautiful that place is untill morning.

Dec 20, 6.30 am, After amazed by the place we slept, everyone went to take bath even in that freezing temperature. At last we thanked everyone in that place and started our journey to ``Yedakumari``. Also we saw few people (most of them are railway contract workers) using that track for transporting. While walking we asked another person how long is ``Yedakumari?`` he said another 10km, we already passed 7km, he also said, there is one small station nearby within 4km from the current place. So we decided to stop in that small station and back to dhonigal. Everything went as we planned this day and we witnessed a beautiful paradise !!!

.. figure:: /_static/images/2010/01/dsc05588.jpg

   Helping hands!!

.. figure:: /_static/images/2010/01/dsc05593.jpg

   Camp Fire!!!

.. figure:: /_static/images/2010/01/dsc05681.jpg

   nearby railway workers house

.. figure:: /_static/images/2010/01/dsc05741.jpg

   First Tunnel

.. figure:: /_static/images/2010/01/dsc05769.jpg

   Second Train - at this time we are inside a tunnel - run!! run!! - amazing moments :) !!

.. figure:: /_static/images/2010/01/dsc05802.jpg

   Longest and Deepest bridge we crossed !! - legs will defenately shake!!

.. figure:: /_static/images/2010/01/dsc05806.jpg

   Mountain View!!

.. figure:: /_static/images/2010/01/dsc05790.jpg

   Did you see any rocks here??

.. figure:: /_static/images/2010/01/dsc05811.jpg

   We reached The Next Small Station!!!

.. figure:: /_static/images/2010/01/dsc05856.jpg

   Back to pavilion - Sakleshpur hotel!!

I'm a person who didn't get opportunity to witness nature's gifts to human, but after visited this place, I just want to spend my life there, people living there love their world, money is not much important to them (we learn that when we gave money for staying one night, that person refused).

There is something which we are gifted, we should protect that gifts till our time and give it to our descendants.

Finally, thanks to hyundai for producing quality product. I love that car which we traveled, its as perfect and gave no trouble in our journey. Even riding it is as enjoyable as nature.

Dec 21, 7.30 am, we are here!! chennai !!. with lot of energy!! happiness!! and full of beautiful memories!! :)

.. figure:: /_static/images/2010/01/dsc05861.jpg

   Thanks Santro !!

Here is the map

.. raw:: html

   <iframe src="https://www.google.com/maps/embed?pb=!1m27!1m12!1m3!1d2683582.0208606217!2d76.35084175618869!3d12.770238670338355!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m12!3e0!4m4!1s0x3a525daa0f6f96f5%3A0x3bcaf93ed3f8426f!3m2!1d12.947878!2d80.2319971!4m5!1s0x3ba52774f409a33b%3A0x19c19dc96452958a!2sDonigal%20Railway%20Station%2C%20Bengaluru%20-%20Mangaluru%20Highway%2C%20Donigal%2C%20Karnataka!3m2!1d12.904370499999999!2d75.7344782!5e0!3m2!1sen!2sin!4v1623042267208!5m2!1sen!2sin" width="600" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
