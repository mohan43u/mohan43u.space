.. title:: Capture screen and mic input using FFmpeg and ALSA

**************************************************
Capture screen and mic input using FFmpeg and ALSA
**************************************************

| Updated: |modifieddate|

.. image:: /_static/images/2010/03/ffmpeg-logo.png
   :align: center
   :target: http://ffmpeg.org/

Recently, interesting commands appeared in `commandlinefu <http://www.commandlinefu.com/commands/matching/x11grab/eDExZ3JhYg==/sort-by-votes>`_ about x11grab using ffmpeg. All are very interesting commands and they did what I expected in my Debian. But only one thing is missing. While creating a screencast, If I want to speak, that should appear in the capturing stream.

Thanks to alsa and ffmpeg, they have a way to capture my laptop's in-built mic input. So I customized the existing commands which appeared in commandlinefu and created a small script.

.. code-block:: bash

   #!/bin/sh
   # simple command to capture your screen
   
   OUTPUT="${1:?[usage] screencast.sh OUTPUTFILE}"
   
   getgeometry()
   {
           xwininfo -root |
           grep 'geometry' |
           awk '{print $2;}'
   }
   
   captureX11()
   {
           ffmpeg -f alsa -itsoffset 00:00:02.000 -ac 2 -i hw:0,0 \
                   -f x11grab -s $(getgeometry) -r 10 -i :0.0 \
                   -sameq -f mp4 \
                   -s wvga \
                   -y "${OUTPUT}"
   }
   
   captureX11

Finally I created my first screencast using ``screencast.sh`` script. Here is the video

.. raw:: html
	 
   <iframe width="852" height="480" src="https://www.youtube.com/embed/K3gOxtXN5wc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Atlast, I don't want to install one more app to create screencast. FFmpeg is with me !!!
