.. title:: Wireless Ad-Hoc Network - Linux

*******************************
Wireless Ad-Hoc Network - Linux
*******************************

| Updated: |modifieddate|

Long time back, I actually did peer-to-peer with RJ232 connector using mine and my friend's laptops. Its really simple and straight forward. Choose a local subnet( probably 192.168.2.0/24), assign 192.168.2.1 to one machine and assign 192.168.2.2 to another machine. Thats all, connection established and I used ``nc`` to transfer files.

This time, its the same old thing, but without RJ232 wire. Also I just configured my system as a gateway so that my friend can access Internet through my laptop.

Before starting, I conformed that my wireless is working,

.. code-block:: console

   $ su -c'iwconfig'
   lo        no wireless extensions.

   eth1      no wireless extensions.

   eth3      IEEE 802.11bg  ESSID:""  
             Mode:Managed  Frequency:2.412 GHz  Access Point: Not-Associated   
             Bit Rate:54 Mb/s   Tx-Power:off   
             Retry min limit:7   RTS thr:off   Fragment thr:off
             Power Managementmode:All packets received
             Link Quality=5/5  Signal level=0 dBm  Noise level=0 dBm
             Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0
             Tx excessive retries:0  Invalid misc:0   Missed beacon:0

   pan0      no wireless extensions.

   eth0      no wireless extensions.

First I announced a Wireless Ad-Hoc with a good name(ESSID ``Dark``), here, there is something called WEP, but I'm still not able to set WEP passphrase through iwconfig. If someone had success through ``iwconfig`` please help me to learn. The below ad-hoc network will not use WEP or any authentication.

.. code-block:: console

   $ su -c'iwconfig eth3 essid 'Dark' mode ad-hoc channel 1'

waited for 2 - 10 seconds, and then tried to connect to that ``Dark`` network.

.. code-block:: console

   $ su -c'iwconfig eth3 essid 'Dark' mode ad-hoc channel 1'

I tried the above command more than one time till I'm associated with a Cell. If you get a mac address in ``Cell``, it means you are now connected to the ``Dark`` Network.

.. code-block:: console

   $ su -c'iwconfig eth3'
   eth3      IEEE 802.11bg  ESSID:"Dark"
	     Mode:Ad-Hoc  Frequency:2.412 GHz  Cell: F6:A5:B9:E6:37:E3
             Bit Rate=54 Mb/s   Tx-Power:32 dBm
             Retry min limit:7   RTS thr:off   Fragment thr:off
             Power Managementmode:All packets received
             Link Quality=5/5  Signal level=-57 dBm  Noise level=-96 dBm
             Rx invalid nwid:0  Rx invalid crypt:0  Rx invalid frag:0
             Tx excessive retries:0  Invalid misc:0   Missed beacon:0

Now, my link-layer is configured, next is to configure the ip layer,

.. code-block:: console

   $ su -c'ifconfig eth3 192.168.2.1 netmask 255.255.255.0'
   $ su -c'ifconfig eth3; route'
   eth3      Link encap:Ethernet  HWaddr 00:1a:73:9f:d3:47  
	     inet addr:192.168.2.1  Bcast:192.168.2.255  Mask:255.255.255.0
             inet6 addr: fe80::21a:73ff:fe9f:d347/64 Scope:Link
             UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
             RX packets:0 errors:0 dropped:0 overruns:0 frame:2
             TX packets:0 errors:6 dropped:0 overruns:0 carrier:0
             collisions:0 txqueuelen:1000 
             RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
             Interrupt:19 

   Kernel IP routing table
   Destination     Gateway         Genmask         Flags Metric Ref    Use Iface
   192.168.2.0     *               255.255.255.0   U     0      0        0 eth3
   192.168.1.0     *               255.255.255.0   U     0      0        0 eth0
   link-local      *               255.255.0.0     U     1000   0        0 eth0
   default         192.168.1.1     0.0.0.0         UG    0      0        0 eth0

Now I configured IP layer, the next part is to configure my debian to route packets,

.. code-block:: console

   $ su -c'sysctl -w net.ipv4.ip_forward=1'
   $ su -c'iptables --verbose --table filter --policy FORWARD ACCEPT'
   $ su -c'iptables --verbose --table nat --append POSTROUTING --jump MASQUERADE'

Note, that the above steps are just for current running kernel, if I want my changes to be permanent, I need to edit /etc/sysctl.conf and I need to save my current iptables using ``iptables-save/iptables-restore``.

Thats all from the gateway machine(that is, my laptop), now I need to add my friend's laptop to this ``Dark`` Network.

.. code-block:: console

   $ sudo iwconfig wlan0 essid "Dark" mode ad-hoc channel 1
   $ sudo ifconfig wlan0 192.168.2.2 netmask 255.255.255.0
   $ sudo route add default gw 192.168.2.1
   $ sudo echo "nameserver 192.168.1.1" >> /etc/resulv.conf

Thats all from my friend's machine, now he just opened firefox and happily browsed from the balcony. No more wires.

Whenever my friends come to my room, they have some bit of tough time accessing internet, becuase most of the time, my laptop will be connected with internet through ADSL. Obviously they have shyness to ask me for stop my surfing. But now, I can happily share my internet connection with my friends. I don't need to buy a wireless router. My laptop itself with the ADSL router doing what a wireless router can do.

Thanks linux.
