.. title:: Support for Broadcom Wireless Devices - Debian - Testing

********************************************************
Support for Broadcom Wireless Devices - Debian - Testing
********************************************************

| Updated: |modifieddate|

Got some time to post this one, I thought I should log my experience, installing wireless support for my broadcom PCI card in debian, so that next time, I will not miss the methods.

Debian provides three way to get support for BCM43XX devices. One is the reverse engineered `b43xx <http://linuxwireless.org/en/users/Drivers/b43>`_ driver comming with the default kernel image. Its opensource and mainly, it works. To get it work, first we need to check whether our device have support from this driver,

.. code-block:: console

   squeeze@Debian:~$ lspci | grep BCM
   03:00.0 Network controller: Broadcom Corporation BCM4311 802.11b/g WLAN (rev 02)
   squeeze@Debian:~$ 

My card have support for b43, I just checked with the list of `PCI cards they provided in their site <http://linuxwireless.org/en/users/Drivers/b43#Known_PCI_devices>`_. Now all I need to do is, install the firmware,

.. code-block:: console

   $ su -c'aptitude install b43-fwcutter'

This will automatically install the firmware. Thus, in your next boot, you will get ``wlan(x)`` interface provided by ``b43`` opensource driver.

If your card is not supported by opensource b43xx driver, still debian provides a good way, its ``broadcom-sta-common`` package. This will install the proprietary ``wl`` driver in a way that it will perfectly sync with debian packaging system.

Before starting this, don't forget to have your backports ready, because debian testing repos have the habit of removing important packages, I had an experience that I was not able to find ``kernel-headers-2.6.30-amd64`` in testing repo, after some googling, I came to know that its available in backports.

.. code-block:: console

   $ su -c'echo "deb http://www.backports.org/debian/ lenny-backports main non-free contrib" >> /etc/apt/sources.list.d/backports.sources.list'
   $ su -c'echo "deb-src http://www.backports.org/debian/ lenny-backports main non-free contrib" >> /etc/apt/sources.list.d/backports.sources.list'
   $ su -c'aptitude update'
   $ su -c'aptitude install debian-backports-keyring'

Now make sure ``module-assistant`` is fully prepared and ready to compile source codes for you.

.. code-block:: console

   $ su -c'module-assistant prepare'

Now we can start our proceedings,

.. code-block:: console

   $ su -c'aptitude install broadcom-sta-common'

Now, we need to compile the proprietary driver using ``module-assistant`` command. This command will download packages to compile source codes on behalf of you using GNU Tools, Debian Build tools etc. Its ncurses based interface will clearly tell you what its doing. select ``broadcom-sta`` in the wizard, ``module-assistant`` will compile and install ``wl`` dirver in this path ``/lib/modules/$(uname -r)/kernel/drivers/net/wireless/wl.ko``

.. code-block:: console

   $ su -c'module-assistant'

Thus, in your next boot, you will get ``eth(x)`` provided by ``wl.ko`` properitory driver. This method is called ``The Debian Way``.

Final method is so simple, `Download the driver from broadcom's website <http://www.broadcom.com/support/802.11/linux_sta.php>`_. Prepare your system to compile source codes. You can do this by installing gcc,autotools,libtool,build-essential etc., or just install and prepare ``module-assistance`` so that your system will be fully capable to compile source codes.

.. code-block:: console

   $ su -c'aptitude install module-assistant'
   $ su -c'module-assistant prepare'

Now just extract, make and make install,

.. code-block:: console

   $ su -c'tar xvzf hybrid-portsrc-x86_64-v5.10.91.9.3.tar.gz'
   $ su -c'make'
   $ su -c'make install'
   $ su -c'echo "blacklist b44" >> /etc/modprobe.d/wl.conf'
   $ su -c'echo "blacklist b43legacy" >> /etc/modprobe.d/wl.conf'
   $ su -c'echo "blacklist b43" >> /etc/modprobe.d/wl.conf'
   $ su -c'echo "blacklist ssb" >> /etc/modprobe.d/wl.conf'
   $ su -c'depmod -aeF /boot/System.map-$(uname -r)'
   $ su -c'update-initramfs -u'

Finally you can see ``wl`` driver in this path ``/lib/modules/$(uname -r)/kernel/drivers/net/wireless/wl.ko``. In next boot you will get ``eth(x)`` wireless interface provided by proprietary ``wl`` driver. In this method, debian will not update this driver for the next kernel updates, you need to do it on your own. This is the main drawback in the final method.
