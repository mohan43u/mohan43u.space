.. title:: Setup Gnome Development Environment in Archlinux

************************************************
Setup Gnome Development Environment in Archlinux
************************************************

| Updated: |modifieddate|

Many people who want to contribute to Gnome will go with Fedora, because it is the best suitable for gnome developer. But, Archlinux is as good as Fedora in terms of providing latest gnome libraries. Here I'll explain how I setup my development environment in Archlinux using `pacstrap <https://wiki.archlinux.org/index.php/beginners%27_guide>`_, `jhbuild <https://wiki.gnome.org/action/show/Projects/Jhbuild?action=show&redirect=Jhbuild>`_ and `arch-chroot. <https://wiki.archlinux.org/index.php/change_root#Using_arch-chroot>`_

Setup Base
==========

In your home directory, create a folder for gnome and install archlinux packages

.. code-block:: console

   $ cd ~/Devel/gnome
   $ su
   # pacstrap -d runtime base base-devel
   # chown -R <username>:<groupname> runtime
   # exit
   $

Here, <username>:<groupname> must be your username and groupname to make sure you are able to modify the files inside ``~/Devel/gnome/runtime`` without any issues. The above steps will install Archlinux base packages. Now, we need to install jhbuild,

Jhbuild
=======

.. code-block:: console

   $ cd ~/Devel/gnome/checkout
   $ git clone git://git.gnome.org/jhbuild
   $ cd jhbuild
   $ PYTHON=/usr/bin/python2 ./autogen.sh --prefix="${HOME}/Devel/gnome/runtime/usr"
   $ make install

done, next step is to configure jhbuild to setup the paths properly, to do this, we need to copy the sample ``jhbuildrc`` file from jhbuild source directory to ``~/.config`` directory and edit that file,

.. code-block:: console

   $ cp ~/Devel/gnome/checkout/jhbuild/examples/sample.jhbuildrc ~/.config/jhbuildrc

Here is my ``.jhbuildrc`` modifications

.. code-block:: diff

   # -*- mode: python -*-
   --- /home/mohan/Devel/gnome/checkout/jhbuild/examples/sample.jhbuildrc   2015-01-17 11:38:40.055187155 +0800
   +++ /home/mohan/.config/jhbuildrc       2015-01-06 09:07:34.493161676 +0800
   @@ -10,22 +10,25 @@
    # jhbuild/defaults.jhbuildrc, but can be any file in the modulesets directory
    # or a URL of a module set file on a web server.
    # moduleset = 'gnome-apps-3.12'
   -#
   +moduleset = 'gnome-world'
   +
    # A list of the modules to build.  Defaults to the GNOME core and tested apps.
    # modules = [ 'meta-gnome-core', 'meta-gnome-apps-tested' ]
    
    # Or to build the old GNOME 2.32:
    # moduleset = 'gnome2/gnome-2.32'
    # modules = ['meta-gnome-desktop']
   +modules = ['gjs', 'farstream', 'gnome-online-accounts', 'polari', 'gnome-shell', 'glade', 'gnome-builder']
    
    # what directory should the source be checked out to?
   -checkoutroot = '~/jhbuild/checkout'
   +checkoutroot = '~/Devel/gnome/checkout'
    
    # the prefix to configure/install modules to (must have write access)
   -prefix = '~/jhbuild/install'
   +prefix = '~/Devel/gnome/runtime/usr'
    
    # custom CFLAGS / environment pieces for the build
    # os.environ['CFLAGS'] = '-Wall -g -O0'
   +os.environ['PYTHON'] = '/usr/bin/python2'
    
    # extra arguments to pass to all autogen.sh scripts
    # to speed up builds of GNOME, try '--disable-static --disable-gtk-doc'

Here is the details of my changes,

moduleset

   A moduleset is an xml file contains details (like modulename, version info, repo address, type of repo, how to build etc.,) of each and every project (we call it as module) needed for gnome environment. jhbuild maintains moduleset for each release, you can see the different modulesets inside your jhbuild directory (``~/Devel/gnome/jhbuild/modulesets``). ``gnome-world`` moduleset represent generic set which will be updated for every release. So, I use ``gnome-world`` to make my environment as latest as possible.

modules

   when we execute ``jhbuild build`` command, jhbuild will compile only the modules listed in this place. make sure you build what you want instead of building all the gnome projects (aka ``meta-gnome-desktop``)

checkoutroot

   place where jhbuild will clone the gnome repositories. set this to ``~/Devel/gnome/checkout``

prefix

   place where jhbuild will put the compiled binaries, set this to ``~/Devel/gnome/runtime/usr``. If you notice, you will see that ``~/Devel/gnome/runtime`` is the same path where we installed archlinux base, so, we basically put the gnome binaries (libraries & executables) on top of archlinux, I'll tell you why we are doing this when I explain arch-chroot.

os.environ['PYTHON']

   set this to ``/usr/bin/python2`` because, Archlinux's default python is python3 but most of gnome modules don't like python3 (especially jhbuild. see `here. <https://wiki.archlinux.org/index.php/JHBuild#Python_issues>`_)

Compiling
=========

All set, its time to compile.

.. code-block:: console

   $ ~/Devel/gnome/runtime/usr/bin/jhbuild build

If jhbuild finds any missing libraries to compile the modules listed in jhbuildrc, it will throw error, here comes the need of arch-chroot.

Installing missing Dependencies
===============================

The reason we setup archlinux base in ``~/Devel/gnome/runtime`` is to install any missing libraries here from arch packages using arch-chroot and packman and make sure we don't distrub the current system. Let say, jhbuild throws that ``spotread`` is missing when compiling colord module. ``spotread`` executable is provided by ``argyllcms`` package and here is the simple way to fix the dependency issue,

.. code-block:: console

   $ cd ~/Devel/gnome
   $ su
   # arch-chroot runtime /usr/bin/bash
   # pacman --sync --refresh
   # pacman --sync --sysupgrade
   # pacman --sync argyllcms
   # exit
   # chown -R <username>:<groupname> runtime
   # exit
   $

Now, we can start compiling again, I suggest to switch to jhbuild environment before we compile, because jhbuild will setup necessary environment variables (like ``LD_LIBRARY_PATH`` pointing to ``~/Devel/gnome/runtime/usr/lib`` etc.,) to make sure dependency libraries are resolved from ``~/Devel/gnome/runtime/usr``.

.. code-block:: console

   $ ~/Devel/gnome/runtime/usr/bin/jhbuild shell
   $ jhbuild build
   $ exit

Now, to execute the latest compiled gnome-builder, here is the commandline

.. code-block:: console

   $ ~/Devel/gnome/runtime/usr/bin/jhbuild run gnome-builder

Thats it, you have your gnome development environment ready.
