.. title:: yield in vala and gio concurrency

*********************************
yield in vala and gio concurrency
*********************************

| Updated: |modifieddate|

By no means, I'm an expert in `Vala <https://wiki.gnome.org/Projects/Vala>`_ or `Gio <https://developer.gnome.org/gio/stable/GSimpleAsyncResult.html>`_. Infact, I'm just a beginner, but I have learned something while playing with vala which I want to record (for future reference). Here I'll explain how Gio's async framework works and how vala uses Gio beneath the tree.

Gio Async Framework
===================

Better to explain with an example,

.. raw:: html

   <script src="https://gist.github.com/mohan43u/954e11ba665de9fd25ab.js"></script>

Here, I have created a new GObject class called ``TestGioAsync`` with ``something_to_say`` property. I have implemented a simple async function called ``test_gio_async_say``. When called, ``test_gio_async_say`` will setup ``GSimpleAsync`` object, attach another function called ``test_gio_async_say_idle_func`` into GMainLoop's next idle iteration and return.

Once ``test_gio_async_say_idle_func`` triggered, it takes the value from ``something_to_say`` property and put it into ``g_simple_async_result_set_op_res_pointer`` as ``op_res`` (which will be picked-up later) and completes the async transaction with ``g_simple_async_result_complete_in_idle`` which will call the ``test_gio_async_say_cb`` provided in ``test_gio_async_say`` in GMainLoop's next idle iteration.

Once ``test_gio_async_say_cb`` triggered, it will call ``test_gio_async_say_finish`` which will simply return ``op_res`` which contains the value taken from ``something_to_say`` property.

Vala Async Framework
====================

This is the equivalent vala example for the above C example

.. raw:: html

   <script src="https://gist.github.com/mohan43u/464ce1b5c83021a4b119.js"></script>

Vala async functions returns immediately whenever it reaches ``yield`` keyword, In this example, I have hooked ``this.say.callback`` into GMainLoop's next idle iteration. In the next idle iteration, ``this.say`` will be called and will start execution exactly after the ``yield`` statement which will eventually trigger the anonymous lambda function provided in  ``this.say.begin`` with ``sentence`` as return value.

Once the lambda function triggered, it will call ``this.say.end`` which will return the ``sentence`` value to the lambda function.

In Vala example, ``this.say.callback`` is equivalent to ``test_gio_async_say_idle_func`` in C example.
