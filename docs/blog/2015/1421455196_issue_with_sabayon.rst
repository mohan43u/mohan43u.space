.. title:: Issue with Sabayon

******************
Issue with Sabayon
******************

| Updated: |modifieddate|

Long time since I blogged. Life is crazy mate (specially after you become a husband). Personals aside, I have been waiting to blog about my experience with Sabayon.

I was a user of sabyon for about two and half years. But now, I'm a Arch user and thats one damn thing I did right to kickstart my hunger for coding again. Well, sorry sabayon developers, you guys suck!! because you dont worry about your users anymore.

Nothing wrong with the distro (I admire the stability it provided), but your attitude towards the users suck!! everytime we report a bug, you people either say raise this bug to upstream (or) raise this bug in gentoo's bugzilla because they are the one who create the package.

If gentoo have a package and we ask it to enable it in sabayon overlay, you will say, we don't enable some package because of one user and one can use gentoo's overlay if they want. Dammit!! if I want to use gentoo's overlay, why do I need to install sabayon?

Anyway, goodbye. I'm now in a true rolling distro.
