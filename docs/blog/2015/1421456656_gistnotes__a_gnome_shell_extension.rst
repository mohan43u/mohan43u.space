.. title:: gistnotes - a gnome shell extension

***********************************
gistnotes - a gnome shell extension
***********************************

| Updated: |modifieddate|

Well, I should probably say, its basically a gnome application disguised as a shell extension. written entirely using javascript.

Nothing much to say more than what the README say here, https://github.com/mohan43u/gistnotes

.. image:: /_static/images/2015/01/screenshot-from-2015-01-17-090253.png

This is my first contribution to gnome. you can install it through https://extensions.gnome.org/extension/917/gistnotes/. Check it out, and provide feedback.
