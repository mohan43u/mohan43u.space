"""modifieddate.

This extension provides `|modifieddate|` substitution

.. moduleauthor:: Mohan R <mohan43u@gmail.com>
"""

import os
import datetime
from docutils import nodes
from docutils.transforms import Transform
from sphinx.util.i18n import format_date
from sphinx.locale import _


class ModifiedDateTransform(Transform):
    """ModifiedDateTransform class."""

    default_priority = 210

    def apply(self, **kwargs):
        """Transform modifieddate substitution."""
        for ref in self.document.traverse(nodes.substitution_reference):
            name = ref['refname']
            if name == 'modifieddate':
                source = self.document['source']
                modifieddate = None
                try:
                    modifieddate = os.stat(source).st_mtime
                except Exception as exception:
                    raise Exception('failed to get modifieddate for %s' % (source)) from exception
                if modifieddate is not None:
                    config = self.document.settings.env.config
                    modifieddate_fmt = config.today_fmt or _('%b %d, %Y')
                    timezone = datetime.timezone.utc
                    modifieddate = datetime.datetime.fromtimestamp(modifieddate,
                                                                   timezone)
                    text = format_date(modifieddate_fmt, date=modifieddate,
                                       language=config.language)
                    ref.replace_self(nodes.Text(text))


def setup(app):
    """extension."""
    app.add_transform(ModifiedDateTransform)
    return {
        'version': '0.0.1',
        'parallel_read_safe': True,
        'parallel_write_safe': True
    }
