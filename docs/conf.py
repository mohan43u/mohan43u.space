# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import os
import sys
sys.path.insert(0, os.path.abspath('exts'))


# -- Project information -----------------------------------------------------

project = "mohan43u's space"
copyright = '2020, Mohan R'
author = 'Mohan R'

# The full version, including alpha/beta/rc tags
release = '0.0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autosectionlabel',
    'yasfb',
    'modifieddate'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = ['_build', 'Thumbs.db', '.DS_Store']

# Footer
rst_epilog = """
|

.. raw:: html

 <script>
  var remark_config = {
   host: 'https://comments.mohan43u.space',
   site_id: 'comments.mohan43u.space',
  }
 </script>
 <script>!function(e,n){for(var o=0;o<e.length;o++){var r=n.createElement("script"),c=".js",d=n.head||n.body;"noModule"in r?(r.type="module",c=".mjs"):r.async=!0,r.defer=!0,r.src=remark_config.host+"/web/"+e[o]+c,d.appendChild(r)}}(remark_config.components||["embed"],document);</script>
 <div id="remark42"></div>

|

.. raw:: html

 <div id="custom-footer">
  <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/80x15.png" /></a> | &copy; 2009 - Present, Mohan R | Powered By <a href="https://www.sphinx-doc.org">Sphinx</a> (<a href="https://alabaster.readthedocs.io">Alabaster Theme</a>)
 </div>
"""

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_title = "mohan43u.space"
html_theme = 'alabaster'
html_theme_options = {
    'logo': '/images/logo.jpg',
    'logo_name': True,
    'logo_text_align': 'center',
    'page_width': '90%',
    'sidebar_width': '20%',
    'font_size': '16px',
    'show_powered_by': False,
    'show_related': True
}

html_sidebars = {
    '**' : [
        'about.html',
        'links.html',
        'searchbox.html',
        'relations.html',
    ]
}

html_css_files = [
]
html_js_files = [
]
html_copy_source = False
html_show_sourcelink = False
html_show_copyright = False
html_show_sphinx = False

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']

# yasfb
feed_base_url = "https://mohan43u.space"
feed_author = "Mohan R"

# latex
latex_engine = 'xelatex'
latex_elements = {
    'fontpkg': r'''
\setmainfont{FreeSerif}[
    Path = /usr/share/fonts/opentype/freefont/,
    BoldFont = *Bold,
    ItalicFont = *Italic
]
\setsansfont{FreeSans}[
    Path = /usr/share/fonts/opentype/freefont/,
    BoldFont = *Bold,
    ItalicFont = *Oblique
]
\setmonofont{FreeMono}[
    Path = /usr/share/fonts/opentype/freefont/,
    BoldFont = *Bold,
    ItalicFont = *Oblique
]
    '''
}
