#ifndef __PLIC_MATH__
#define __PLIC_MATH__

#ifdef __cplusplus
extern "C" {
#endif

  int plic_add(int a, int b);
  int plic_sub(int a, int b);
  int plic_mul(int a, int b);

#ifdef __cplusplus
}
#endif

#endif /* __PLIC_MATH__ */
