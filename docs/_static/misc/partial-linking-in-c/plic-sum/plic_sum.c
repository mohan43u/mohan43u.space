#include <stdio.h>
#include <plic_math.h>
#include "plic_sum.h"

int plic_sum(int i, int j) {
  int n = 0;
  int sum = 0;

  for(n = i; n < j; n++) {
    sum = plic_add(sum, n);
  }

  return sum;
}
