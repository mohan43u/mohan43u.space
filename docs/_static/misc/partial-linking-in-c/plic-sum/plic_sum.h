#ifndef __PLIC_SUM__
#define __PLIC_SUM__

#ifdef __cplusplus
extern "C" {
#endif

  int plic_sum(int i, int j);

#ifdef __cplusplus
}
#endif

#endif /* __PLIC_MATH__ */
